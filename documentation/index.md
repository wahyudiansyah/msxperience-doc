# MSXperience Back-end

Selamat datang di aplikasi *back-end* untuk MSXperience.

## Daftar Isi

* [Spesifikasi](documentation/index.md#spesifikasi)
* [Development (non-Windows)](documentation/index.md#development-non-windows)
* [Development (Windows)](documentation/index.md#development-windows)
* [Environment Variable](documentation/index.md#environment-variable)
* [Konfigurasi](documentation/index.md#konfigurasi)
* [Deployment](documentation/index.md#deployment)
* [Troubleshooting](documentation/index.md#troubleshooting)
* [Startup](documentation/index.md#startup)
* [Cron](documentation/index.md#cron)
    * [Fungsi / Service yang Memicu Cron](documentation/index.md#fungsi-service-yang-memicu-cron)
* [Jobs](documentation/index.md#jobs)
    * [checkScheduledJobs](documentation/index.md#checkscheduledjobs)
    * [fetchCdr](documentation/index.md#fetchcdr)
    * [fetchCmr](documentation/index.md#fetchCmr)
    * [cleanDownloads](documentation/index.md#cleandownloads)
    * [cleanLogs](documentation/index.md#cleanlogs)
    * [fetchCallManagers](documentation/index.md#fetchcallmanagers)
    * [fetchWebex](documentation/index.md#fetchwebex)
    * [fetchWebexHostUser](documentation/index.md#fetchwebexhostuser)
    * [fetchTeams](documentation/index.md#fetchTeams)
* [Services](documentation/index.md#services)
    * [Areas](documentation/index.md#areas)
    * [Bandwidth Capacity](documentation/index.md#bandwidth-capacity)
    * [Branch Type](documentation/index.md#branch-type)
    * [Call Termination](documentation/index.md#call-termination)
    * [CUCMAXLs](documentation/index.md#cucmaxls)
    * [Download](documentation/index.md#download)
    * [Endpoint Capacity](documentation/index.md#endpoint-capacity)
    * [Endpoint Effectivity](documentation/index.md#endpoint-effectivity)
    * [Fetch](documentation/index.md#fetch)
    * [License Capacity](documentation/index.md#license-capacity)
    * [Pools](documentation/index.md#pools)
    * [Report Dashboard](documentation/index.md#report-dashboard)
    * [Return of Investment (ROI)](documentation/index.md#return-of-investment-roi)
    * [Settings](documentation/index.md#settings)
    * [Sites](documentation/index.md#sites)
    * [Teams Usage](documentation/index.md#teams-usage)
    * [Test](documentation/index.md#test)
    * [Webex Usage](documentation/index.md#webex-usage)

## Spesifikasi

|  |  |
| --- | --- |
| Bahasa Pemrograman | JavaScript |
| *Runtime* | [Node.js v10 (LTS, dubnium)](https://nodejs.org) |
| *Framework* | [FeathersJS](https://feathersjs.com) |
| *Database* | [PostgreSQL 10](https://www.postgresql.org/) |
| Penyimpanan *Cache* | [Redis](https://redis.io/) |
| *Deployment Tool* | [Docker (use docker-compose)](https://www.docker.com/) |

## Development (non-Windows)

1. Install Node.js (direkomendasikan versi 10 LTS).

> Disarankan menggunakan [*Node Version Manager* (NVM)](https://github.com/nvm-sh/nvm).
> Alternatifnya, dapat menggunakan *package management* (apt-get, yum, dnf, brew) untuk OS tersebut.
> 
> Jika menggunakan [NVM](https://github.com/nvm-sh/nvm), *install* versi 10 (`lts/dubnium`).
> Cara *install* Node.js versi 10 melalui [NVM](https://github.com/nvm-sh/nvm):
> 
> `$ nvm install lts/dubnium`

2. Install [PostgreSQL 10](https://www.postgresql.org/download/).

> Disarankan menggunakan [Docker](https://www.docker.com/) untuk mempermudah dalam meng-*install* PostgreSQL.
> Alternatifnya, dapat menggunakan *package management* (`apt-get`, `yum`, `dnf`, `brew`) untuk OS tersebut.
> 
> Jika menggunakan Docker, pastikan *port* **5432** tidak ada yang menggunakannya dan menjalankan *file* `PostgreSQL_Docker.sh`.

3. Install [Redis](https://redis.io).

> Disarankan menggunakan [Docker](https://www.docker.com/) untuk mempermudah dalam meng-*install* Redis.
> 
> Jika menggunakan Docker, pastikan *port* **6379** tidak ada yang menggunakannya dan menjalankan *file* `Redis_Docker.sh`.

4. *Clone repository*-nya.

> `$ git clone https://gitlab.com/heriance/msxperience-be`

5. Masuk ke direktori *repository* yang telah di-*clone*.

> `$ cd msxperience-be/`

6. Jalankan `npm ci`

> `$ npm ci`
> 
> Jika `$ npm ci` gagal karena `node-gyp` tidak bisa melakukan *compile*, *install package* berikut.
> 
> 
> > Debian dan turunannya:
> > `$ sudo apt install build-essential python`
> > 
> > Red Hat dan turunannya:
> > `$ sudo yum install python make gcc gcc-c++`
> > 
> > Referensi: [node-gyp](https://github.com/nodejs/node-gyp)

7. Jalankan `npm run dev`

> `$ npm run dev`

8. (Opsional) Untuk melihat *cache* dengan GUI, pastikan *port* **8081** tidak terpakai dan jalankan skrip `RedisCommander_Docker.sh`.

> `$ ./RedisCommander_Docker.sh`

## Development (Windows)

1. *Install* [Laragon Full](https://laragon.org/download/).

> Cara ini direkomendasikan karena *setup*-nya yang mudah untuk *development*)

2. Install [Node.js](https://nodejs.org/) di Laragon. (direkomendasikan versi 10 LTS) dan atur sebagai *default*.
3. Install [PostgreSQL 10](https://www.postgresql.org/download/) di Laragon, pastikan *port* **5432** tidak digunakan, dan nyalakan *service*-nya.
4. Pastikan *port* **6379** tidak digunakan dan nyalakan *service* [Redis](https://redis.io).
5. *Clone repository*-nya.

> `$ git clone https://gitlab.com/heriance/msxperience-be`

6. Masuk ke direktori *repository* yang telah di-*clone*.

> `$ cd msxperience-be/`

7. Jalankan `npm ci`

> `$ npm ci`
> 
> Jika `$ npm ci` gagal karena `node-gyp` tidak bisa melakukan *compile*, gunakan [langkah-langkah berikut](https://github.com/nodejs/node-gyp#on-windows).

8. Jalankan `npm run dev`

> `$ npm run dev`

9. (Opsional) Untuk melihat *cache* dengan GUI, di Laragon dapat menggunakan *Web admin* Redis yang tersedia.

## Environment Variable

Berikut adalah *environment variable* yang digunakan pada aplikasi ini.

| Variabel | Nilai | Deskripsi |
| -------- | ----- | ---------------------------------------------------- |
| `NODE_ENV` | `development` | Environment yang dijalankan pada aplikasi. |
| `DEBUGGING` | `1` | Untuk menyalakan mode debug. Hapus variabel ini jika tidak ingin menggunakannya. |
| `SKIP_AUTH` | `1` | Untuk mengabaikan autentikasi ke back-end Mastersystem. Hapus variabel ini jika tidak ingin menggunakannya. |
| `CRONJOB` | `1` | Untuk menjalankan aplikasi sebagai cronjob (khusus mengolah data yang dikirim oleh back-end Mastersystem). Hapus variabel ini jika tidak ingin menggunakannya. |

## Konfigurasi

Untuk mengatur koneksi antara aplikasi dengan \_cache, dan *database*, berikut nama-nama *file* berdasarkan NODE\_ENV nya.

* `config/default.json`: Untuk environment *default* atau environment-nya tidak memiliki *file* konfigurasinya.
* `config/docker.json`: untuk environment DOCKER. (Tidak digunakan)
* `config/staging.json`: untuk environment STAGING.
* `config/production.json`: untuk environment PRODUCTION.

Berikut adalah konfigurasi yang ada di dalam file tersebut.

| Konfigurasi | Tipe | Nilai | Deskripsi |
| ----------- | ---- | ----- | ---------------------------------------------------------- |
| `host` | `string` | `"0.0.0.0"` | Alamat IP yang diperbolehkan untuk mengakses aplikasi |
| `port` | `integer` | `3030` | Nomor port yang dibuka untuk mendengarkan *request*. |
| `enableTestCase` | `boolean` | `false` | Opsi yang digunakan untuk menjalankan Endpoint Test Case. |
| `public` | `string` | `"../public"` | Lokasi halaman depan (index). |
| `authString` | `string` | `"randomString"` | Sebuah string yang digunakan sebagai autentikasi ketika melakukan *request* ke aplikasi (<b>HANYA *PRODUCTION*</b>) |
| `logDateFormat` | `Object` |  | Konfigurasi untuk menentukan format penanggalan pada *log*, baik di nama *file* ataupun konten pada *log*. |
| `logDateFormat.fileName` | `string` | `YYYY-MM-DD_HH-mm-ss` | Format penanggalan pada nama *file log*. |
| `logDateFormat.contentFile` | `string` | `YYYY-MM-DD_HH-mm-ss` | Format penanggalan pada konten *file log*. |
| `postgres` | `Object` |  | Konfigurasi untuk menghubungkan aplikasi dengan *database*. |
| `postgres.schema` | `string` | `"\\public"` | Menentukan nama *schema* pada *database*. Disarankan menambahkan *backslash* agar tidak mengambil nilai *schema* dari *environment variable*. |
| `postgres.url` | `string` | `"postgres://postgres@127.0.0.1:5432/msxperience"` | URL yang digunakan untuk mnentukan lokasi *database* dan nama *database* yang diinginkan. |
| `postgres.pool` | `Object` |  | Konfigurasi untuk menentukan jumlah koneksi yang digunakan pada 1 *instance* aplikasi ini. |
| `postgres.pool.max` | `integer` | `10` | Jumlah maksimal koneksi yang dapat dibuat dalam 1 *pool*. |
| `postgres.pool.min` | `integer` | `1` | Jumlah minimal koneksi yang ditetapkan (*stand-by*) dalam 1 *pool*. |
| `postgres.pool.idle` | `integer` | `10000` | Waktu maksimum, dalam milidetik, untuk menahan koneksi yang sedang *idle* sebelum di-*disconnect*. |
| `postgres.pool.acquire` | `integer` | `60000` | Waktu maksimum, dalam milidetik, untuk mencoba mengambil koneksi yang tersedia dari pool sebelum melempar error. |
| `mastersystem` | `Object` |  | Konfigurasi untuk menghubungkan aplikasi dengan *database* Mastersystem. |
| `mastersystem.schema` | `string` | `"\\public"` | Menentukan nama *schema* pada *database*. Disarankan menambahkan *backslash* agar tidak mengambil nilai *schema* dari *environment variable*. |
| `mastersystem.url` | `string` | `"postgres://postgres@127.0.0.1:5432/msxperience"` | URL yang digunakan untuk mnentukan lokasi *database* dan nama *database* yang diinginkan. |
| `mastersystem.pool` | `Object` |  | Konfigurasi untuk menentukan jumlah koneksi yang digunakan pada 1 *instance* aplikasi ini. |
| `mastersystem.pool.max` | `integer` | `10` | Jumlah maksimal koneksi yang dapat dibuat dalam 1 *pool*. |
| `mastersystem.pool.min` | `integer` | `1` | Jumlah minimal koneksi yang ditetapkan (*stand-by*) dalam 1 *pool*. |
| `mastersystem.pool.idle` | `integer` | `10000` | Waktu maksimum, dalam milidetik, untuk menahan koneksi yang sedang *idle* sebelum di-*disconnect*. |
| `mastersystem.pool.acquire` | `integer` | `60000` | Waktu maksimum, dalam milidetik, untuk mencoba mengambil koneksi yang tersedia dari pool sebelum melempar error. |
| `redis` | `Object` |  | Konfigurasi untuk menghubungkan aplikasi dengan Redis. |
| `redis.host` | `string` | `"127.0.0.1"` | Lokasi server Redis dijalankan. |
| `redis.port` | `string` | `"6379"` | Nomor port yang digunakan pada server Redis. |
| `ciscoWebex` | `Object` |  | Konfigurasi untuk alamat URL yang berhubungan dengan Cisco Webex. |
| `ciscoWebex.reqAccessTokenUrl` | `string` | `"https://api.ciscospark.com/v1/access_token"` | URL untuk mendapatkan *Access Token* dari Cisco Webex. |
| `ciscoWebex.reqLicensesUrl` | `string` | `"https://api.ciscospark.com/v1/licenses"` | URL untuk mendapatkan lisensi Webex Host User dari Cisco Webex. |

## Deployment

Untuk men-*deploy* aplikasi back-end ini, disarankan menggunakan [Docker](https://www.docker.com) dengan menjalankan docker-compose. Pastikan [Docker](https://www.docker.com) dan [docker-compose](https://docs.docker.com/compose/) telah ter-*install*.

1. Jalankan file `prod.build-msxp-web.sh`.

> `$ ./prod.build-msxp-web.sh`

2. Jalankan file `deploy.sh`.

> `$ ./deploy.sh`

## Troubleshooting

Setelah melakukan proses deployment, terkadang kita perlu melakukan pengecekan maupun pembaruan aplikasi jika terdapat update atau bug fixing. Untuk melakukannya kita memerlukan pengetahuan tentang docker, dan berikut adalah command atau perintah praktis yang mungkin berguna untuk memecahkan masalah.

### Check Container running

> docker ps -a

![docker_ps_-a.png](images/docker_ps_-a.png)

untuk mengetahui apakah ada container yang bermasalah


### Restart / Update apps

> docker restart {container name}

![docker_restart](images/docker_restart.png)

Container atau aplikasi yang telah berjalan tidak serta merta akan melakukan update otomatis meskipun terdapat penambahan code pada direktori src nya. Aplikasi ini dijalankan menggunakan node.js (node command) sehingga kita perlu merestart run process untuk memulai ulang aplikasi menggunakan code terbaru.

Untuk merestart container yang menjalankan cron misalnya:

```
docker restart msxp-datalab_cronjob_1
```

Perlu diketahui juga semua container akan melakukan restart jika server direstart sehingga container akan mengambil code terbaru.

### Logs / Debugging apps

Kita bisa menemukan log dari aplikasi ini pada direktori `logs`. Direktori ini berisi file-file terkait pencatatan dari sebuah proses tertentu, misal logs untuk mengambilan data cmr maupun cdr. Setiap file log juga telah diberi format tanggal dan waktu untuk mengetahui kapan log tersebut dicatat sehingga kita mudah untuk melacak proses yang bermasalah.

Selain file-file log pada direktori `logs`, kita juga bisa melacak secara langsung pada container yang sedang berjalan dengan menggunakan perintah berikut:

> docker logs {container name}

![docker_logs](images/docker_logs.png)

Untuk memudahkan pemecahan masalah, aplikasi telah dirancang untuk menampilkan log pada proses-proses yang dirasa cukup vital, sebagai contoh proses cron maupun startup. Kita dengan mudah bisa memantau jalannya proses dengan menjalankan perintah logs. Dengan perintah ini diharapkan kesalahan cepat ditemukan penyebabnya dan cepat teratasi



## Startup

Proses startup melalui rangkaian beberapa proses sebagai berikut

1. memuat file konfigurasi
2. menjalankan konfigurasi redis
3. menjalankan konfigurasi database
4. menjalankan konfigurasi middleware
5. menjalankan konfigurasi service
6. menjalankan konfigurasi application hooks
7. menjalankan server
8. menjalankan cron

### File Konfigurasi

Untuk environment production NODE\_ENV=production file konfigurasinya adalah sebagai berikut:
<br>
```
{
  "host": "0.0.0.0",
  "port": 3030,
  "enableTestCase": true,
  "public": "../public/",
  "authString": "mRiLfF3CksShMK6h10ybyJBwYjJnaJSRa5Pm0pMCPwWkiGO49JOeXjnzTolWXFyRuE6bPHM8V9BJgANEdYbNTYTR6IUd7ozmuqR3kpW18zbefOSEKPtub5khs0UJoSORbPwye2GiM67W98II6EjSh4MUM6mF1Oug3j9abBwnJX1gXeo6TV4bgWfFG8LeK25CqwWY3Qk2rD6mHAuGiuASb9iMNiShwmvIwLFvhv61Vj1q6qWxzbIRyFnu9cgBh9nIFIub9rRSY8kqVDooPmnv2Bn2tZ7fdjsu9AV8MgxrdbVyOexIyLA783GvEtM18Ir3ilo9T2YMJrrnkNre8HPfUiJFv5UUhgVNWgB3rOWzdhaiKqX1IToQn5iYYj8PcHOjUkTksG78aeBXYxeWh1r5b3ywDz5Tc4bQGO2uxn1l2jtArtCeXJTHj5x3FuqFnLlXNeDnTxXp0mNlfjKalKMMV74jVnAgpCpkZzTIbNlbfcPV6N6nEvz1guZ6v47Qsi7uB8JIsjoCOk7T7PgjG0WHlg3kq1YFKPBQ8Hx9CTQqhiVySJsXUVL2Ky0d5OeiLGgyFjVM2BxgozRpboAYmnIB3YQcwBn9EUDRKYjTnueiJhpKVZauXXOGZdXboqg5zwHBOMPmRzesAzzen9MNlW9wt0GieWBHopU3ynCvlzck44zl65gFKbfww1GSonM7rqfvRhXcNdvczRiUV0X2sIdD0Z4D31RNVO3zEYeYNXTT7MyHAwTdAKEdRWi7HHyOE6CT1rckk5ESKit1BWZUUFIEEXCzkJ96d3uWArKLlldHCZeWlEhZe9G6T6XQBDfGOpeUtFVJTiKLqIOsRT4mpkCIwwXfc1chN7FRAxjflcDnIWICsSvmQrPiAuV2Qatt6iD9aKUXNWbyHdMsnkGimE27XGGxyPAcf9UuE8ymQz5BsI5cwx2AkLeeplASZp58jgukdNEa0ic2gC5jVUKDiId9Y7DdqEJ2zXVsjEESPdER9LOYNapH7PsCtckbMYqdKBh3BhqQKncbsHuume6Nevo7lpvBdM5p5gDZbdesxB9zae4PQJnnvUYdF3sXB6m5DeoKPsaHfIH07pyCBJ5ZXZsVNKhFwJz6YTaThfM90tE6xQM5ec9bMydgptgUvdDuyfqzWfCrUVfz6oPZ9jYixb4RCrp3Nl93PO57YwTNePk5YjG3RZdsDHbTYn2t1aNgoxiIqmXWr8KepDjQ8HLAbJBf3jrQB9TLNjFPzVtp2ngqgujukTF5rwAIZXaSbjAgJynVx424UGC35nEd23fuizNUK23T84KZyjWX6f9uMOhzxq4OkU2Iy8DIECMzgd81j1kmrFCj6BqbJUVQQ0JDRlAsVk66aTHOL9kl1p5ECMbjdgQtfBNcYW9Xxe9MqLC4tbQo5MUcZsbaAWeqIzjMcaLLobqFiWPTOFGzI78iFvuOiy9r4fxDFOqWLm84CPbNtNHBY2DxIf63HFsl93isgq3php6AKc6De37p4qNajmK5yHUnBD7K2V9VtPT0Xwgpe5nfoTgtso40KpNZebIZES49MgLSXkaK7smcrTwC9YgQQRL73HZ6dVaCHA6S4Ex3XPgcKgeTe9NKt6ETWmBQ4na0PFQJGsryRQNQ2hN3pb63f06qhozKTYSCMnwFr09OH5WZibQkRj7fFmAq725KUutsz7hO5UQtXdOIHq54AQKlUzxhalPilhO0Nw3juNLPvRG5MSIvUFqAIc6Ez8hvitI4OKcfIajDMs8U4NE5lrQO1ToAStdMnxz4WEOnIB8NwwRxK6jjEOZVH3rH8mKeSmy58lqfMjdRLjY3oFhU1mkjuaYGOHsWd2dILPAbLoQPffy8Ee9Gl7k5xsM5RKvjrLg4aS5MNLDCWk5EJQtbrXwn9aEpgjiFKhBgJQLkbgiOL57465Wb9jCJ5ogJWwTW6v9HxJFtpHqHX6PRZNXrLMcl5BCq2gdV1BPXS7EMlX1IVv92lF5CfFqhRQQilHqQiJQA9JtwUi8FIIaVNHct2As6k7BJqRYUrFNyyAzWLApvXRDw",
  "paginate": {    "default": 10,
    "max": 50
  },
  "logDateFormat": {    "fileName": "YYYY-MM-DD_HH-mm-ss",
    "contentFile": "YYYY-MM-DD HH:mm:ss"
  },
  "postgres": {    "schema": "\\public",
    "url": "postgres://postgres@database:5432/msxperience",
    "pool": {      "max": 100,
      "min": 0,
      "idle": 90000,
      "acquire": 90000
    }  },
  "mastersystem": {    "schema": "\\public",
    "url": "postgres://postgres@database:5432/msxperience",
    "pool": {      "max": 10,
      "min": 0,
      "idle": 30000,
      "acquire": 30000
    }  },
  "redis" : {    "host": "cache",
    "port": "6379"
  },
  "ciscoWebex": {    "reqAccessTokenUrl": "https://api.ciscospark.com/v1/access_token",
    "reqLicensesUrl": "https://api.ciscospark.com/v1/licenses"
  }}
```

### menjalankan konfigurasi redis

Server redis pada aplikasi ini digunakan untuk menyimpan cache hasil query dari front-end.

### menjalankan konfigurasi database

Database pada aplikasi ini menggunakan PostgreSQL sehingga memerlukan konfigurasi terkait endpoint database dan authentikasi ke server databasenya.

### menjalankan konfigurasi middleware

Di dalam konfigurasi middleware ini jika environment yang dijalankan adalah production maka aplikasi akan dipaksa untuk menggunakan authentikasi mastersystem. Client yang mengakses aplikasi pada mode production diharuskan mencantumkan field 'x-auth-string' pada 'HEADER' http request, value dari field ini didapat dari file config 'authString' di atas.

```
'HEADERS':
'x-auth-string' : 'mRiLfF3CksShMK6h10ybyJBwYjJnaJSRa5Pm0pMCPwWkiGO49JOeXjnzTolWXFyRuE6bPHM8V9BJgANEdYbNTYTR6IUd7ozmuqR3kpW18zbefOSEKPtub5khs0UJoSORbPwye2GiM67W98II6EjSh4MUM6mF1Oug3j9abBwnJX1gXeo6TV4bgWfFG8LeK25CqwWY3Qk2rD6mHAuGiuASb9iMNiShwmvIwLFvhv61Vj1q6qWxzbIRyFnu9cgBh9nIFIub9rRSY8kqVDooPmnv2Bn2tZ7fdjsu9AV8MgxrdbVyOexIyLA783GvEtM18Ir3ilo9T2YMJrrnkNre8HPfUiJFv5UUhgVNWgB3rOWzdhaiKqX1IToQn5iYYj8PcHOjUkTksG78aeBXYxeWh1r5b3ywDz5Tc4bQGO2uxn1l2jtArtCeXJTHj5x3FuqFnLlXNeDnTxXp0mNlfjKalKMMV74jVnAgpCpkZzTIbNlbfcPV6N6nEvz1guZ6v47Qsi7uB8JIsjoCOk7T7PgjG0WHlg3kq1YFKPBQ8Hx9CTQqhiVySJsXUVL2Ky0d5OeiLGgyFjVM2BxgozRpboAYmnIB3YQcwBn9EUDRKYjTnueiJhpKVZauXXOGZdXboqg5zwHBOMPmRzesAzzen9MNlW9wt0GieWBHopU3ynCvlzck44zl65gFKbfww1GSonM7rqfvRhXcNdvczRiUV0X2sIdD0Z4D31RNVO3zEYeYNXTT7MyHAwTdAKEdRWi7HHyOE6CT1rckk5ESKit1BWZUUFIEEXCzkJ96d3uWArKLlldHCZeWlEhZe9G6T6XQBDfGOpeUtFVJTiKLqIOsRT4mpkCIwwXfc1chN7FRAxjflcDnIWICsSvmQrPiAuV2Qatt6iD9aKUXNWbyHdMsnkGimE27XGGxyPAcf9UuE8ymQz5BsI5cwx2AkLeeplASZp58jgukdNEa0ic2gC5jVUKDiId9Y7DdqEJ2zXVsjEESPdER9LOYNapH7PsCtckbMYqdKBh3BhqQKncbsHuume6Nevo7lpvBdM5p5gDZbdesxB9zae4PQJnnvUYdF3sXB6m5DeoKPsaHfIH07pyCBJ5ZXZsVNKhFwJz6YTaThfM90tE6xQM5ec9bMydgptgUvdDuyfqzWfCrUVfz6oPZ9jYixb4RCrp3Nl93PO57YwTNePk5YjG3RZdsDHbTYn2t1aNgoxiIqmXWr8KepDjQ8HLAbJBf3jrQB9TLNjFPzVtp2ngqgujukTF5rwAIZXaSbjAgJynVx424UGC35nEd23fuizNUK23T84KZyjWX6f9uMOhzxq4OkU2Iy8DIECMzgd81j1kmrFCj6BqbJUVQQ0JDRlAsVk66aTHOL9kl1p5ECMbjdgQtfBNcYW9Xxe9MqLC4tbQo5MUcZsbaAWeqIzjMcaLLobqFiWPTOFGzI78iFvuOiy9r4fxDFOqWLm84CPbNtNHBY2DxIf63HFsl93isgq3php6AKc6De37p4qNajmK5yHUnBD7K2V9VtPT0Xwgpe5nfoTgtso40KpNZebIZES49MgLSXkaK7smcrTwC9YgQQRL73HZ6dVaCHA6S4Ex3XPgcKgeTe9NKt6ETWmBQ4na0PFQJGsryRQNQ2hN3pb63f06qhozKTYSCMnwFr09OH5WZibQkRj7fFmAq725KUutsz7hO5UQtXdOIHq54AQKlUzxhalPilhO0Nw3juNLPvRG5MSIvUFqAIc6Ez8hvitI4OKcfIajDMs8U4NE5lrQO1ToAStdMnxz4WEOnIB8NwwRxK6jjEOZVH3rH8mKeSmy58lqfMjdRLjY3oFhU1mkjuaYGOHsWd2dILPAbLoQPffy8Ee9Gl7k5xsM5RKvjrLg4aS5MNLDCWk5EJQtbrXwn9aEpgjiFKhBgJQLkbgiOL57465Wb9jCJ5ogJWwTW6v9HxJFtpHqHX6PRZNXrLMcl5BCq2gdV1BPXS7EMlX1IVv92lF5CfFqhRQQilHqQiJQA9JtwUi8FIIaVNHct2As6k7BJqRYUrFNyyAzWLApvXRDw'
```

### menjalankan konfigurasi service

Service bisa disebut juga endpoint untuk menjalankan fungsi tertentu. Di dalam aplikasi ini memuat banyak service.

### menjalankan konfigurasi application hooks

Hooks adalah fungsi yang bisa dijalankan diluar service tapi masih di dalam area timeline service, bisa sebelum service, sesudah service, atau ketika service menghasilkan error. Hooks pada level aplikasi dimaksudkan untuk dijalankan di semua service di dalam aplikasi. Sebuah Hook bisa saja dijalankan untuk kasus service tertentu, tapi untuk application hooks maka fungsi-fungsi ini akan dijalankan di semua service sesuai dengan timeline dimana dia ditempatkan (before/after/error)

Hooks pada level aplikasi adalah sebagai berikut:
* log
* checkService
* checkCache
* checkError
* checkPaginate 

#### log
Hooks `log` ditempatkan di semua timeline service baik itu sebelum, sesudah maupun ketika service error. Hal ini dimaksudkan agar setiap menjalankan service dapat segera diketahui apabila terjadi masalah.

#### checkService
`Checkservice` digunakan untuk mengecek apakah sebuah service boleh dihit dari luar atau sebagai service internal yang hanya boleh dihit dari dalam.


#### checkCache
`CheckCache` biasanya dipakai oleh service public, dimaksudkan untuk mengecek apakah sebelumnya sudah ada data query yang tersimpan di dalam cache. Jika sudah ada cache maka response tidak perlu lagi diambil dari process query melainkan langsung dari cache nya saja.

#### checkPaginate
Hooks ini untuk mengubah value yang dikirim dari query string menjadi boolean jika terdapat field `paginate` di dalam query string tersebut


### menjalankan server

Server dijalankan sesuai dengan config 'host' dan 'port' pada file config dalam hal ini jika mode production yang digunakan maka server akan berjalan pada host '0.0.0.0' dan port 3030

### menjalankan cron

cron dijalankan sesudah server dinyalakan, fungsi untuk menjankan cron merupakan callback dari

```
server.on('listening' [,callback])
```

pada praktiknya di mode production cron job dirunning pada container atau instance yang berbeda dengan instance web application

## Cron

Dijalankan setiap menit

* [checkScheduledJobs](/documentation/index.md#checkscheduledjobs)

Dijalankan setiap jam 1 malam

* [cleanDownloads](/documentation/index.md#cleandownloads)
* [cleanLogs](documentation/index.md#cleanlogs)

Setiap 10 menit, dari jam 9 pagi hingga 4 sore, di hari Senin - Jumat

* [fetchCallManagers](documentation/index.md#fetchcallmanagers)

Setiap jam 12 malam di awal bulan

* [fetchWebex](documentation/index.md#fetchwebex)
* [fetchWebexHostUser](documentation/index.md#fetchwebexhostuser)

Setiap 10 menit

* [fetchCmsLicense](/documentation/index.md#fetchcmslicense)
* [fetchCmsCoSpace](/documentation/index.md#fetchcmscospace)

Setiap hari Minggu jam 11 malam

* [refreshWebexToken](/documentation/index.md#refreshWebexToken)

Setiap hari jam 12.05 malam

* [fetchTeams](documentation/index.md#fetchTeams)

`commented` Setiap sepuluh menit pada jam 8 sampai 16 di hari Senin - Jum'at

* [fetchCmsMultiPartyLicense](documentation/index.md#fetchCmsMultiPartyLicense)

### Fungsi / Service yang Memicu Cron

sebelum masuk ke job-job yang dijalankan secara periodik, perlu diketahui service / fungsi atau endpoint yang membentuk dan membuat job-job ini berjalan.
Jobs yang hanya berjalan dikarenakan pemicu dari luar hanya ada dua yaitu

1. fetchCdr
2. fetchCmr

selain dari jobs di atas dipastikan tidak memerlukan perintah dari luar untuk menjalankan job nya.
kedua jobs tersebut dipicu oleh enpoint / service berikut ini

1. POST fetch/cdr => untuk memicu job fetchCdr
2. POST fetch/cmr => untuk memicu job fetchCmr

## Jobs

### checkScheduledJobs

checkScheduledJobs adalah fungsi yang dirunning setiap menitnya untuk mengecek job-job yang belum dirunning atau job yang gagal dirunning.
job yang dijalankan dari fungsi ini adalah:

1. fetchCdr
2. fetchCmr
3. fetchWebex
4. fetchWebexHostUser

### fetchCdr
Job ini berfungsi untuk menyusun data di tabel fact untuk menyajikan data pada service endpoint-capacity, bandwidth-capacity, license-capacity, return-of-investment, dan service sites. Datanya didapat dari data CDR, CMR dan difilter menggunakan CUCM

### fetchCmr
Job ini berfungsi untuk menyusun data di tabel fact untuk menyajikan data pada service endpoint-capacity, bandwidth-capacity, license-capacity, return-of-investment, dan service sites. Datanya didapat dari data CDR, CMR dan difilter menggunakan CUCM


### cleanDownloads

Directory './downloads' merupakan directory untuk menyimpan secara temporary file yang didownload oleh client. File-file ini setiap harinya harus dibersihkan untuk mengosongkan penyimpanan. File yang berada di dalam directory downloads biasanya merupakan file reporting.

### cleanLogs

directory './logs' merupakan directory untuk menyimpan response dari cron yang mengambil data ke server lain. File-file response ini setiap harinya harus dibersihkan untuk mengosongkan penyimpanan.

### fetchCallManagers
Job ini berfungsi untuk menyusun data di tabel people_counts. Data ini digunakan untuk menyajikan data pada service endpoint-effectivity dan service return-of-investment.

### fetchWebex

fetchWebex adalah fungsi untuk mengumpulkan data webex selama 1 bulan pada bulan sebelumnya. Contoh hasil fetchWebex dari tanggal 01-03-2020 sampai 31-03-2020 bisa dilihat pada file berikut [fetchWebex.json](documentation/json/fetchWebex.json). Data ini kemudian disimpan ke dalam table 'webex' dan dipakai untuk menampilkan data webex-usage.

### fetchWebexHostUser

fetchWebexHostUser adalah fungsi untuk mengambil data lisensi Host pengguna aplikasi webex sehingga memungkinkan untuk perhitungan penggunaan aplikasi webex pada service webex-usage

### refreshWebexToken

Untuk bisa mendapatkan jumlah lisensi Host pengguna aplikasi webex diperlukan token awal dan refresh token, refreshWebexToken berguna untuk mendapatkan kembali token baru agar selalu bisa terkoneksi dengan api webex.

### fetchCmsLicense

Job ini berfungsi untuk mengambil lisensi yang tersedia.

### fetchCmsCoSpace

Job untuk mengambil CoSpace dari API nya CMS

### fetchTeams

Job ini berfungsi untuk mengambil data-data teams, terkait data teams fungsinya dibagi lagi menjadi 3:
* fetchTeamsUser
* fetchTeamsDevice
* fetchTeamsActivity

### fetchCmsMultiLicensing

Job ini digunakan untuk mendapatkan jumlah lisensi dari CMS API

## Services

Aplikasi ini memiliki beberapa jenis *service*.

| Jenis Service | Deskripsi |
| ------------- | --------------------------------------------------------------------------------- |
| `Deprecated` | *Service* yang segera tidak akan mendapatkan *support* untuk rilis yang akan datang. Namun, masih dapat digunakan. |
| `External` | *Service* yang hanya digunakan untuk melakukan *sync database* di luar aplikasi ini. |
| `Internal` | *Service* yang hanya dapat dipanggil melalui service lainnya. Contoh: *Service* `endpoint-capacity` dapat memanggil *service* `jobs`, namun *service* `jobs` tidak dapat dipanggil melalui *browser* atau sejenisnya. |
| `Public` | *Service* yang dapat dipanggil dari luar aplikasi (*browser*) dan dipanggil dari *service* lainnya. |

Berikut adalah beberapa *service public* yang dapat digunakan untuk mendapatkan data berdasarkan *service*-nya.

| Service | Deskripsi |
| ------- | --------------------------------------------------------------------------------------- |
| Areas | Untuk mendapatkan data area. |
| Bandwidth Capacity | Untuk mendapatkan jumlah penggunaan *bandwidth* yang digunakan oleh suatu *site*. |
| CUCMAXLs | Untuk mendapatkan data endpoint yang diperoleh dari *server* CUCM. |
| Download | Untuk mendapatkan laporan penggunaan pada modul tertentu. |
| Endpoint Capacity | Untuk mendapatkan jumlah penggunaan setiap endpoint di semua *site* atau *site* tertentu. |
| Endpoint Effectivity | Untuk mendapatkan jumlah penggunaan endpoint yang mendukung People Count dan memberikan saran jika penggunaannya dikatakan tidak efektif. |
| Fetch | Untuk mengirim sebuah *job* yang akan dilakukan oleh cronjob. |
| License Capacity | Untuk mendapatkan jumlah penggunaan lisensi yang di mana ada *endpoint* yang melakukan penelponan ke server CMS. |
| Pools | Untuk mendapatkan data pools. |
| Return of Investment (ROI) | Untuk menghitung jumlah uang yang dapat dihemat dengan mengganti *meeting* secara fisik (bepergian) dengan menggunakan perangkat Cisco. |
| Settings | Untuk melakukan konfigurasi pengaturan yang berhubungan dengan mendapatkan data Cisco. |
| Sites | Untuk mendapatkan data sites. |
| Webex Usage | Untuk mendapatkan data penggunaan Webex. |

## Areas

### GET
> /areas

Untuk mendapatkan list data area menggunakan pagination

| parameter | tipe | deskripsi | opsional | contoh | default |
| --------- | ---- | --------- | -------- | ------ | ------- |
| $limit | integer | batas jumlah pencarian | ya | 1 | 10 |
| $skip | integer | jumlah data pagination yang dilewati | ya | 10 | 0 |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/areas?$limit=1&$skip=0'
```
</details>

<details>
<summary>Response</summary>


|json|deskripsi|
|----|-----------|
|`{`|
|`   "total": 5,` | jumlah data jika tanpa limit dan skip |
|`    "limit": 1,` | batas pengambilan data |
|`    "skip": 0,`  | jumlah data yang dilewati untuk pagination |
|`    "data": [ ...` | data dalam bentuk array |
|`        {`                                                      |
|`            "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`      |
|`            "name": "Wilayah 1",`                               |
|`            "created": "2020-04-13T11:41:35.434Z",`             |
|`            "updated": "2020-04-13T11:41:35.434Z",`             |
|`            "destinations": [ ...`                              | site-site lain yang tersambung dengan site ini |
|`                {`|
|`                    "id": "35be8b8b-fcd0-4385-857b-997ca0a08f28",`|
|`                    "name": "Wilayah 5",`|
|`                    "created": "2020-04-13T11:41:35.434Z",`|
|`                    "updated": "2020-04-13T11:41:35.434Z",`|
|`                    "area_costs": {`|
|`                        "cost": "666570.00",`|
|`                        "created": "2020-04-13T11:41:35.454Z",`|
|`                        "updated": "2020-04-13T11:41:35.454Z",`|
|`                        "from": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`                        "to": "35be8b8b-fcd0-4385-857b-997ca0a08f28"`|
|`                    }`|
|`                },`|
|`            ],`|
|`            "pools": [ ...`| data endpoint yang terdapat pada sites ini |
|`                {`|
|`                    "name": "DP_VICON_TGL",`|
|`                    "description": "Vicon Tegal",`|
|`                    "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`                    "created": "2020-04-13T11:41:35.476Z",`|
|`                    "updated": "2020-04-13T11:41:35.476Z",`|
|`                    "deleted": null`|
|`                },`|
|`            ]`|
|`        }`|
|`    ]`|
|`}`|

</details>

            


> /areas/:id

Untuk medapatkan satu data area berdasarkan id

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/areas/eafbd335-1275-4ac4-a92a-11c2f38b7e69'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`    "name": "Wilayah 1",`|
|`    "created": "2020-04-13T11:41:35.434Z",`|
|`    "updated": "2020-04-13T11:41:35.434Z",`|
|`    "destinations": [ ...`| site-site lain yang tersambung dengan site ini |
|`        {`|
|`            "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "name": "Wilayah 1",`|
|`            "created": "2020-04-13T11:41:35.434Z",`|
|`            "updated": "2020-04-13T11:41:35.434Z",`|
|`            "area_costs": {`|
|`                "cost": "872038.00",`|
|`                "created": "2020-04-13T11:41:35.454Z",`|
|`                "updated": "2020-04-13T11:41:35.454Z",`|
|`                "from": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`               "to": "eafbd335-1275-4ac4-a92a-11c2f38b7e69"`|
|`            }`|
|`        },`|
|`    ],`|
|`    "pools": [ ...`| jumlah enpoint yang terdapat pada site ini |
|`        {`|
|`            "name": "DP_KNR",`|
|`            "description": "Kendari",`|
|`            "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "created": "2020-04-13T11:41:35.476Z",`|
|`            "updated": "2020-04-13T11:41:35.476Z",`|
|`            "deleted": null`|
|`        }`|
|`    ]`|
|`}`|

</details>

### POST

> /areas

untuk menambahkan data baru ke tabel areas

<details>
<summary>Body</summary>

```
{
	"name": "Wilayah X"
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request POST 'http://localhost:3030/areas' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
	"name": "Wilayah X"
}'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "created": "2020-04-27T03:21:16.610Z",`| datetime site dibuat |
|`    "updated": "2020-04-27T03:21:16.610Z",`| datetime site diupdate |
|`    "id": "8133fb59-52dc-4dd2-bca8-5a727a1678da",`| id site |
|`    "name": "Wilayah X"`| nama site |
|`}

</details>

### PATCH

> /areas

untuk mengubah data pada tabel areas

<details>
<summary>Body</summary>

```
[
	{
		"id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",
		"name": "Wilayah 1a"
	}
]
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/areas' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '[
	{
		"id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",
		"name": "Wilayah 1a"
	}
]'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`[ ...`| data-data yang diupdate dalam bentuk array |
|`    {`| ditail data yang diupdate |
|`        "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`        "name": "Wilayah 1a",`|
|`        "created": "2020-04-13T11:41:35.434Z",`|
|`        "updated": "2020-04-27T03:32:57.741Z"`|
|`    },`|
|`]`|

</details>

### DELETE

> /areas/:id

untuk menghapus sebuah data area menggunakan id nya

<details>
<summary>Curl</summary>

```
curl --location --request DELETE 'http://localhost:3030/areas/1d5976b1-1dc3-4d3d-84a3-6b1784014165' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
	"name": "Wilayah X"
}'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`| ditail data yang dihapus |
|`    "id": "1d5976b1-1dc3-4d3d-84a3-6b1784014165",`|
|`    "name": "Wilayah 4",`|
|`    "created": "2020-04-13T11:41:35.434Z",`|
|`    "updated": "2020-04-13T11:41:35.434Z"`|
|`}`|

</details>

## Bandwidth Capacity

### GET

> /bandwidth-capacity

Menghasilkan nilai dari penggunaan bandwidth di semua sites dalam range tanggal tertentu.

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`  "result": {`|
|`    "DP_BDG": {`| site |
|`      "01-04-2020": {`| data untuk site pada tanggal tersebut
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|
|`        },`|
|`        "totalEndpoint": 1`| jumlah endpoint pada site |
|`      },`|
|`      "cucmaxl": {`|
|`        "name": "SEP881DFC610341",`|
|`        "description": "DX70 Bandung",`|
|`        "model": "Cisco DX70",`|
|`        "devicePoolName": "DP_BDG",`|
|`        "location": "Hub_None",`|
|`        "misconfigured": false,`|
|`        "bandwidth": "2048",`|
|`        "created": "2020-04-13T11:41:38.827Z",`|
|`        "updated": "2020-04-13T11:41:38.827Z",`|
|`        "deleted": null,`|
|`        "device": {`|
|`          "id": "2",`|
|`          "model": "Cisco DX70",`|
|`          "minCapacity": 1,`|
|`          "maxCapacity": 1,`|
|`          "category": 0,`|
|`          "created": "2020-04-13T11:41:38.765Z",`|
|`          "updated": "2020-04-13T11:41:38.765Z",`|
|`          "deleted": null`|
|`       },`|
|`        "pool": {`|
|`          "name": "DP_BDG",`|
|`          "description": "Bandung",`|
|`          "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`          "created": "2020-04-13T11:41:35.476Z",`|
|`          "updated": "2020-04-13T11:41:35.476Z",`|
|`          "deleted": null,`|
|`          "Area": {`|
|`            "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "name": "Wilayah 1",`|
|`            "created": "2020-04-13T11:41:35.434Z",`|
|`            "updated": "2020-04-13T11:41:35.434Z"`|
|`          }`|
|`        }`|
|`      },`|
|`      "lowUsage": {`|
|`        "data": [],`| jumlah data low usage |
|`        "highest": {`| bandwitdh tertinggi pada low usage |
|`          "voice": 0,`|
|`          "video": 0,`|
|`          "presentation": 0,`|
|`          "totalUsage": 0`|
|`        }`|
|`      },`|
|`      "normalUsage": {`| 
|`        "data": [],`| jumlah data normal usage |
|`        "highest": {`| bandwitdh tertinggi pada normal usage |
|`          "voice": 0,`|
|`          "video": 0,`|
|`          "presentation": 0,`|
|`          "totalUsage": 0`|
|`        }`|
|`      },`|
|`      "highUsage": {`| 
|`        "data": [`| jumlah data high usage |
|`          {`| ditail data |
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          }`|
|`        ],`|
|`        "highest": {`| bandwith tertinggi pada high usage |
|`          "date": "01-04-2020",`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        }`|
|`      },`|
|`      "totalEndpoints": 1,`|
|`      "totalNotEndpoints": 0,`|
|`      "highestPerDate": {`| resume high usage per tanggal |
|`        "01-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "02-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "03-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "06-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "07-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "08-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        }`|
|`      },`|
|`      "highestUsage": {`| high usage dari semua tanggal |
|`        "date": "01-04-2020",`|
|`        "voice": 310.2,`|
|`        "video": 107.89999999999999,`|
|`        "presentation": 220.60000000000005,`|
|`        "totalUsage": 638.7,`|
|`        "readableName": "DX70 Bandung",`|
|`        "callingPartyNumber": "1127",`|
|`        "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`      },`|
|`      "02-04-2020": {`| date berikutnya |
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|
|`        },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "03-04-2020": {`|
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|
|`        },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "06-04-2020": {`|
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|
|`        },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "07-04-2020": {`|
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|
|`        },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "08-04-2020": {`|
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|
|`        },`|
|`        "totalEndpoint": 1`|
|`      }`|
|`    }`|
|`  },`|
|`  "weekdays": 6`| jumlah hari kerja dalam seminggu (sabtu minggu tidak dihitung) |
|`}`|

</details>

<details>
<summary>Curl</summary>

```
curl 'http://localhost:3030/bandwidth-capacity?startDate=2020-04-01&endDate=2020-04-08&paginate=false' -H 'Connection: keep-alive' -H 'Accept: application/json, text/plain, */*' -H 'Origin: http://localhost:4200' -H 'Sec-Fetch-Dest: empty' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36' -H 'Sec-Fetch-Site: same-site' -H 'Sec-Fetch-Mode: cors' -H 'Referer: http://localhost:4200/bandwidth-capacity' -H 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7' --compressed
```
</details>

## Branch Type

### GET
>/branch-type

Untuk mendapatkan data tipe-tipe cabang dari semua sites, apakah itu Kantor Cabang Utama, Kantor Cabang Pembantu atau Kantor Pusat.

| parameter | tipe | deskripsi | opsional | contoh | default |
| --------- | ---- | --------- | -------- | ------ | ------- |
| $limit | integer | batas jumlah pencarian | ya | 1 | 10 |
| $skip | integer | jumlah data pagination yang dilewati | ya | 10 | 0 |

<details>
<summary>Curl</summary>

    curl --location --request GET 'http://localhost:3030/branch-type'
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`  "total": 3,`| total semua data jika tidak ada limit atau skip |
|`  "limit": 10,`| limit data yang diambil |
|`  "skip": 0,`| skip data untuk pagination |
|`  "data": [...`| data dalam bentuk array | 
|`    {`| ditail data |
|`      "id": "a804f5d8-4e2c-4b3c-a0f9-2ddc5de55527",`| id cabang |
|`      "name": "Kantor Cabang Utama",`| nama cabang |
|`      "created": "2020-01-31T07:13:24.629Z",`| data dibuat pada (datetime) |
|`      "updated": "2020-02-10T11:43:08.220Z",`| data diubah pada (datetime) |
|`      "branch": [ ...`| data devices yang ada pada branch ini dalam bentuk array |
|`        {`| ditail device |
|`          "id": "28",`|
|`          "model": "Cisco TelePresence MX200 G2",`|
|`          "minCapacity": 5,`|
|`          "maxCapacity": 10,`|
|`          "category": 0,`|
|`          "created": "2019-08-19T07:55:00.990Z",`|
|`          "updated": "2019-08-19T07:55:00.990Z",`|
|`          "deleted": null,`|
|`          "endpoint_cost": {`| biaya untuk endpoint ini |
|`            "cost": "123450.00",`|
|`            "created": "2020-02-03T04:06:16.872Z",`|
|`            "updated": "2020-02-03T04:06:16.872Z",`|
|`            "model": "28",`|
|`            "type": "a804f5d8-4e2c-4b3c-a0f9-2ddc5de55527"`|
|`          }`|
|`        }`|
|`      ],`|
|`      "pools": [ ...`| data sites atau pools yang ada pada cabang ini, dalam bentuk array |
|`        {`| ditail pool |
|`          "name": "PL_DP",`|
|`          "description": "PL_DP",`|
|`          "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`          "timezone": "GMT+07:00",`|
|`          "created": "2019-08-22T04:11:28.339Z",`|
|`          "updated": "2020-02-03T03:58:28.488Z",`|
|`          "deleted": null,`|
|`          "branch_type": "a804f5d8-4e2c-4b3c-a0f9-2ddc5de55527"`|
|`        }`|
|`      ]`|
|`    },`|
|`    {`|
|`      "id": "ba7a157b-1cbe-4fe5-b13d-ab7b138073f5",`|
|`      "name": "Kantor Pusat",`|
|`      "created": "2020-01-31T07:10:03.530Z",`|
|`      "updated": "2020-01-31T08:47:30.366Z",`|
|`      "branch": [ ...`|
|`        {`|
|`          "id": "28",`|
|`          "model": "Cisco TelePresence MX200 G2",`|
|`          "minCapacity": 5,`|
|`          "maxCapacity": 10,`|
|`          "category": 0,`|
|`          "created": "2019-08-19T07:55:00.990Z",`|
|`          "updated": "2019-08-19T07:55:00.990Z",`|
|`          "deleted": null,`|
|`          "endpoint_cost": {`|
|`            "cost": "6000000.00",`|
|`            "created": "2020-02-12T06:44:28.568Z",`|
|`            "updated": "2020-02-12T06:44:28.568Z",`|
|`            "model": "28",`|
|`            "type": "ba7a157b-1cbe-4fe5-b13d-ab7b138073f5"`|
|`          }`|
|`        }`|
|`      ],`|
|`      "pools": [ ...`|
|`        {`|
|`          "name": "WA_Vicon_DP",`|
|`          "description": "WA_Vicon_DP",`|
|`          "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`          "timezone": "GMT+07:00",`|
|`          "created": "2019-08-22T04:11:03.944Z",`|
|`          "updated": "2020-02-03T03:58:28.493Z",`|
|`          "deleted": null,`|
|`          "branch_type": "ba7a157b-1cbe-4fe5-b13d-ab7b138073f5"`|
|`        }`|
|`      ]`|
|`    },`|
|`    {`|
|`      "id": "e8021b06-6a8a-424a-af1e-b26bb6fffae6",`|
|`      "name": "Kantor Cabang Pembantu",`|
|`      "created": "2020-01-31T09:30:39.205Z",`|
|`      "updated": "2020-02-10T11:43:08.215Z",`|
|`      "branch": [ ... ],`|
|`      "pools": [`|
|`        {`|
|`          "name": "WA_DP",`|
|`          "description": "WA_DP",`|
|`          "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`          "timezone": "GMT+07:00",`|
|`          "created": "2019-08-22T04:10:57.248Z",`|
|`          "updated": "2020-02-03T03:58:28.492Z",`|
|`          "deleted": null,`|
|`          "branch_type": "e8021b06-6a8a-424a-af1e-b26bb6fffae6"`|
|`        }`|
|`      ]`|
|`    }`|
|`  ]`|
|`}`|


</details>

>/branch-type/:uuid

untuk mendapatkan single data branch-type dengan menggunakan id nya

<details>
<summary>Curl</summary>

    curl --location --request GET 'http://localhost:3030/branch-type/a804f5d8-4e2c-4b3c-a0f9-2ddc5de55527'
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`| ditail data |
|`    "id": "a804f5d8-4e2c-4b3c-a0f9-2ddc5de55527",`| id branch |
|`    "name": "Kantor Cabang Utama",`| nama branch |
|`    "created": "2020-01-31T07:13:24.629Z",`| dibuat pada (datetime) |
|`    "updated": "2020-02-10T11:43:08.220Z",`| diubah pada (datetime) |
|`    "branch": [...`| device atau endpoint yang ada pada branch ini |
|`        {`| detail endpoint |
|`            "id": "2",`|
|`            "model": "Cisco DX70",`|
|`            "minCapacity": 1,`|
|`            "maxCapacity": 1,`|
|`            "category": 0,`|
|`            "created": "2019-08-19T07:55:00.990Z",`|
|`            "updated": "2019-08-19T07:55:00.990Z",`|
|`            "deleted": null,`|
|`            "endpoint_cost": {`| biaya endpoint |
|`                "cost": "1110.00",`|
|`                "created": "2020-01-31T09:20:47.465Z",`|
|`                "updated": "2020-02-03T03:54:02.635Z",`|
|`                "model": "2",`|
|`                "type": "a804f5d8-4e2c-4b3c-a0f9-2ddc5de55527"`|
|`            }`|
|`        }`|
|`    ],`|
|`    "pools": [...`| sites atau pools yang ada pada branch ini dalam bentuk array |
|`        {`| ditail pool |
|`            "name": "demo_vicon",`|
|`            "description": "demo_vicon",`|
|`            "area": "a6acd463-1826-4c54-bf5a-eb1d43669c44",`|
|`            "timezone": "GMT+07:00",`|
|`            "created": "2019-09-04T08:01:00.847Z",`|
|`            "updated": "2020-01-31T08:19:17.985Z",`|
|`            "deleted": null,`|
|`            "branch_type": "a804f5d8-4e2c-4b3c-a0f9-2ddc5de55527"`|
|`        },`|
|`    ]`|
|`}`|
</details>

### POST
>/branch-type

untuk menambahkan single data branch

<details>
<summary>Body</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`	"name":"cabang 1"`| nama cabang |
|`}`|
</details>

<details>
<summary>Curl</summary>

```
curl --location --request POST 'http://localhost:3030/branch-type' \
--header 'Content-Type: application/json' \
--data-raw '{
	"name":"cabang 1"
}'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "created": "2020-05-15T06:28:35.637Z",`| dibuat pada (datetime) |
|`    "updated": "2020-05-15T06:28:35.637Z",`| diubah pada (datetime) |
|`    "id": "bad6f3cb-7918-4b2d-8c81-2e1d05d4466b",`| id cabang |
|`    "name": "cabang 1"`| nama cabang |
|`}`|
</details>

### PUT
>/branch-type/:uuid

Digunakan untuk mengubah single data branch menggunakan id nya

<details>
<summary>Body</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`	"name":"cabang 2"`| nama cabang |
|`}`|
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PUT 'http://localhost:3030/branch-type/bad6f3cb-7918-4b2d-8c81-2e1d05d4466b' \
--header 'Content-Type: application/json' \
--data-raw '{
	"name":"cabang 2"
}'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "id": "bad6f3cb-7918-4b2d-8c81-2e1d05d4466b",`| id cabang |
|`    "name": "cabang 2",`| nama cabang |
|`    "created": "2020-05-15T06:28:35.637Z",`| dibuat pada (datetime) |
|`    "updated": "2020-05-15T06:37:19.908Z"`| diubah pada (datetime) |
|`}`|
</details>

### PATCH
>/branch-type/

Digunakan untuk mengubah secara massal branch-branch yang ada sekaligus mengatur cost terhadap endpoint yang ada di dalam cabang tersebut

<details>
<summary>Body</summary>

|json|deskripsi|
|----|-----------|
|`[...`| data dalam bentuk array |
|`	      {`|
|`            "id": "bad6f3cb-7918-4b2d-8c81-2e1d05d4466b",`| id branch |
|`            "name": "cabang2",`| nama branch |
|`            "cost": 1000`| const yang akan diapply pada semua endpoint yang ada pada branch tersebut |
|`        },`|
|`        {`|
|`            "id": "ae3cde28-8bd5-415c-91a8-4edd86ab67ee",`|
|`            "name": "cabang 1",`|
|`            "cost": 2000`|
|`        }`|
|`]`|

</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/branch-type/' \
--header 'Content-Type: application/json' \
--data-raw '[
	{
            "id": "bad6f3cb-7918-4b2d-8c81-2e1d05d4466b",
            "name": "cabang2",
            "cost": 1000
        },
        {
            "id": "ae3cde28-8bd5-415c-91a8-4edd86ab67ee",
            "name": "cabang 1",
            "cost": 2000
        }
	]'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`[...`| data dalam bentuk array |
|`    {`|
|`        "id": "bad6f3cb-7918-4b2d-8c81-2e1d05d4466b",`| id branch |
|`        "name": "cabang2",`| nama branch |
|`        "created": "2020-05-15T06:28:35.637Z",`| dibuat pada (datetime) |
|`        "updated": "2020-05-15T06:49:11.568Z"`| diubah pada (datetime |
|`    },`|
|`    {`|
|`        "id": "ae3cde28-8bd5-415c-91a8-4edd86ab67ee",`|
|`        "name": "cabang 1",`|
|`        "created": "2020-05-15T06:46:02.017Z",`|
|`        "updated": "2020-05-15T06:49:11.562Z"`|
|`    }`|
|`]`|
</details>

### DELETE
>/branch-type/:uuid

Digunakan untuk menghapus single data branch menggunakan id nya

<details>
<summary>Curl</summary>

```
curl --location --request DELETE 'http://localhost:3030/branch-type/bad6f3cb-7918-4b2d-8c81-2e1d05d4466b'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|  
|`    "id": "bad6f3cb-7918-4b2d-8c81-2e1d05d4466b",`| id branch |
|`    "name": "cabang2",`| nama branch |
|`    "created": "2020-05-15T06:28:35.637Z",`| dibuat pada (datetime) |
|`    "updated": "2020-05-15T06:49:11.568Z"`| diubah pada (datetime) |
|`}`|
</details>

## Call Termination

### GET
>/call-termination

Digunakan untuk mengambil list data jenis-jenis penyebab sebuah panggilan teleconverence diakhiri 

| parameter | tipe | deskripsi | opsional | contoh | default |
| --------- | ---- | --------- | -------- | ------ | ------- |
| $limit | integer | batas jumlah pencarian | ya | 1 | 10 |
| $skip | integer | jumlah data pagination yang dilewati | ya | 10 | 0 |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/call-termination'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "total": 137,`| total keseluruhan data jika tidak dilimit atau diskip |
|`    "limit": 10,`| batas jumlah pengambilan data |
|`    "skip": 0,`| skip untuk pagination |
|`    "data": [...`| data dalam bentuk array |
|`        {`|
|`            "id": "0",`| id call termination |
|`            "description": "No error",`| deskripsi penyebab call termination |
|`            "created": "2020-02-18T05:03:04.636Z",`| dibuat pada (datetime) |
|`            "updated": "2020-02-18T05:03:04.636Z"`| diubah pada (datetime) |
|`        },`|
|`        {`|
|`            "id": "1",`|
|`            "description": "Unallocated (unassigned) number",`|
|`            "created": "2020-02-18T05:03:04.636Z",`|
|`            "updated": "2020-02-18T05:03:04.636Z"`|
|`        }`|
|`    ]`|
|`}`|
</details>

>/call-termination/:id

Digunakan untuk mengambil single data call termination berdasarkan id nya

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/call-termination/1'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "id": "1",`| id data |
|`    "description": "Unallocated (unassigned) number",`| deskripsi call termination |
|`    "created": "2020-02-18T05:03:04.636Z",`| dibuat pada (datetime) |
|`    "updated": "2020-02-18T05:03:04.636Z"`| diubah pada (datetime) |
|`}`|
</details>

### POST
>/call-termination

Digunakan untuk menambah single data call termination

<details>
<summary>Body</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`	"id": "150",`| id data |
|`	"description": "description 2"`| deskripsi call termination |
|`}`|
</details>

<details>
<summary>Curl</summary>

```
curl --location --request POST 'http://localhost:3030/call-termination' \
--header 'Content-Type: application/json' \
--data-raw '{
	"id": "150",
	"description": "description 2"
}'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "created": "2020-05-15T07:20:52.594Z",`| dibuat pada (datetime) |
|`    "updated": "2020-05-15T07:20:52.594Z",`| diubah pada (datetime) |
|`    "id": "150",`| id data |
|`    "description": "description 2"`| deskripsi call termination |
|`}`|
</details>


### PUT
>/call-termination/:id

Digunakan untuk mengubah single data mengunakan id nya

<details>
<summary>Body</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`	"id": 150,`| id data |
|`	"description": "description 6"`| deskripsi call termination |
|`}`|
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PUT 'http://localhost:3030/call-termination/150' \
--header 'Content-Type: application/json' \
--data-raw '{
	"id": 150,
	"description": "description 6"
}'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "id": "150",`| id data |
|`    "description": "description 6",`| deskripsi call termination |
|`    "created": "2020-05-15T07:20:52.594Z",`| dibuat pada (datetime) |
|`    "updated": "2020-05-15T08:58:08.034Z"`| diubah pada (datetime) |
|`}`|
</details>

### PATCH
>/call-termination/:id

Digunakan untuk mengubah sebagian field dari single data call termination menggunakan id nya

<details>
<summary>Body</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "description": "description 7",`| value yang akan diterapkan pada data |
|`}`|
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/call-termination/150' \
--header 'Content-Type: application/json' \
--data-raw '{
	"description": "description 7"
}'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "id": "150",`| id data |
|`    "description": "description 7",`| deskripsi call termination |
|`    "created": "2020-05-15T07:20:52.594Z",`| dibuat pada (datetime) |
|`    "updated": "2020-05-15T09:50:51.234Z"`| diubah pada (datetime) |
|`}`|
</details>

### DELETE
>/call-termination/:id

digunakan untuk menghapus single data call termination berdasarkan id nya

<details>
<summary>Curl</summary>

```
curl --location --request DELETE 'http://localhost:3030/call-termination/150'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "id": "150",`| id data |
|`    "description": "description 7",`| deskripsi call termination |
|`    "created": "2020-05-15T07:20:52.594Z",`| dibuat pada (datetime) |
|`    "updated": "2020-05-15T09:50:51.234Z"`| diubah pada (datetime) |
|`}`|
</details>

## CUCMAXLs

### GET 
> /cucmaxls

Untuk mendapatkan data list endpoint mengunakan pagination

| parameter | tipe | deskripsi | opsional | contoh | default |
| --------- | ---- | --------- | -------- | ------ | ------- |
| $limit | integer | batas jumlah pencarian | ya | 1 | 10 |
| $skip | integer | jumlah data pagination yang dilewati | ya | 10 | 0 |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/cucmaxls?$limit=10&$skip=0'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "total": 110,`| jumlah data jika tanpa limit dan skip |
|`    "limit": 10,`| batas pengambilan data |
|`    "skip": 0,`| jumlah data yang dilewati untuk pagination |
|`    "data": [ ...`| data dalam bentuk array |
|`        {`| ditail data |
|`            "name": "SEPE4C7226466F8",`| nama endpoint |
|`            "description": "EX90 8225 KP 1",`|
|`            "model": "Cisco TelePresence EX90",`|
|`            "devicePoolName": "DP_JAKARTA01",`|
|`            "location": "LOC_JAKARTA02",`|
|`            "misconfigured": false,`|
|`            "bandwidth": "2048",`|
|`            "created": "2020-04-13T11:41:38.827Z",`|
|`            "updated": "2020-04-13T11:41:38.827Z",`|
|`            "deleted": null,`|
|`            "device": {`|
|`                "id": "25",`|
|`                "model": "Cisco TelePresence EX90",`|
|`                "minCapacity": 0,`|
|`                "maxCapacity": 0,`|
|`                "category": 0,`|
|`                "created": "2020-04-13T11:41:38.765Z",`|
|`                "updated": "2020-04-13T11:41:38.765Z",`|
|`                "deleted": null`|
|`            },`|
|`            "pool": {`| pool endpoint terhubung |
|`                "name": "DP_JAKARTA01",`|
|`                "description": "Kantor Pusat 1",`|
|`                "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`                "created": "2020-04-13T11:41:35.476Z",`|
|`                "updated": "2020-04-13T11:41:35.476Z",`|
|`                "deleted": null,`|
|`                "Area": {`|
|`                    "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`                    "name": "Wilayah 1a",`|
|`                    "created": "2020-04-13T11:41:35.434Z",`|
|`                    "updated": "2020-04-27T03:32:57.741Z"`|
|`                }`|
|`            }`|
|`        }`|
|`    ]`|
|`}`|
</details>

## Download

### GET
> /download/endpoint-capacity

Untuk mengunduh file report dari endpoint-capacity menggunakan filter range date

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/download/endpoint-capacity?startDate=2020-04-01&endDate=2020-04-08' \
--header 'Connection: keep-alive' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Origin: http://localhost:4200' \
--header 'Sec-Fetch-Dest: empty' \
--header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36' \
--header 'Sec-Fetch-Site: same-site' \
--header 'Sec-Fetch-Mode: cors' \
--header 'Referer: http://localhost:4200/endpoint-capacity' \
--header 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7'
```
</details>

<details>
<summary>Response</summary>

```
Response dari request ini adalah unduhan berupa file xlsx
```
</details>

> /download/endpoint-effectivity

Untuk mengunduh file report dari endpoint-effectivity menggunakan filter date dan sebuah endpoint

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| endpoint | string | code endpoint | tidak | "SEP5C838FDC0F50" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/download/endpoint-effectivity?date=2019-07-08&endpoint=SEP5C838FDC0F50'
```
</details>

<details>
<summary>Response</summary>


Response dari request ini adalah unduhan berupa file xlsx dengan format sebagai berikut


| Date | Time | Site | Device SN | Endpoint | Min. Capacity | Max. Capacity | People Counts | Crowd Status |
| ---- | ---- | ---- | --------- | -------- | ------------- | ------------- | ------------- | ------------ |
|      |      |      |           |          |               |               |               |              |    

--------------------------------
</details>

> /download/bandwidth-capacity

Untuk mengunduh file report dari bandwidth-capacity menggunakan filter range date

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/download/bandwidth-capacity?startDate=2019-06-01&endDate=2019-06-08'
```
</details>

<details>
<summary>Response</summary>

```
Response dari request ini adalah unduhan berupa file xlsx
```
</details>

> /download/license-capacity

Untuk mengunduh file report dari license-capacity menggunakan filter range date

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/download/license-capacity?startDate=2019-07-01&endDate=2019-07-12'
```
</details>

<details>
<summary>Response</summary>


Response dari request ini adalah unduhan berupa file xlsx dengan format seperti berikut

| Date | Connected Time | Disconnected Time	| Calling Party Number | Final Called Party Number | In-hour Usage |
| ---- | -------------- | ----------------- | -------------------- | ------------------------- | ------------- |
---------------------------
</details>

> /download/webex-usage

Untuk mengunduh file report dari webex-usage menggunakan filter range date

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/download/webex-usage?startDate=2019-06-01&endDate=2019-06-30'
```
</details>

<details>
<summary>Response</summary>


Response dari request ini adalah unduhan berupa file xlsx dengan format sebagai berikut 

| Date | Host Name | Host WebEx ID | Conference ID | Participant Name | Participant Type | Meeting Start Time | Meeting End Time | Join Time | Leave Time | Total Participants | Participant Duration (minutes) | Meeting Duration (minutes) |
| ---- | --------- | ------------- | ------------- | ---------------- | ---------------- | ------------------ | ---------------- | --------- | ---------- | ------------------ | ------------------------------ | -------------------------- |
-----------------------
</details>

> /download/cost-saving

Untuk mengunduh file report dari analisa biaya yang dihemat (cost saving) dengan menggunakan filter date range dan pool

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |
| origDevicePoolName | string | filter berdasarkan pool, jika kosong maka data diambil dari semua pools | ya | "B2B_DP" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/download/cost-saving?startDate=2020-05-01&endDate=2020-05-18&origDevicePoolName=B2B_DP' \
--header 'Connection: keep-alive' \
--header 'Accept: application/json, text/plain, */*' \
--header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36' \
--header 'Origin: http://localhost:4200' \
--header 'Sec-Fetch-Site: same-site' \
--header 'Sec-Fetch-Mode: cors' \
--header 'Sec-Fetch-Dest: empty' \
--header 'Referer: http://localhost:4200/user/cost-saving' \
--header 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7'
```
</details>

<details>
<summary>Response</summary>

Response dari request ini adalah unduhan berupa file xlsx dengan format sebagai berikut 

| ID | Date | Connected Time | Disconnected Time | Caller | Receiver | Duration | Caller Device SN | Receiver Device SN | People |
|----|------|----------------|-------------------|--------|----------|----------|------------------|--------------------|--------|									
|    |									
|    |									
|    |									
|    |									
| Conventional Cost | 0 |								
| Teleconference Cost | 0 |								
| Delta Cost | 0 |								


</details>

> /download/dashboard-usage

Untuk mengunduh file report terkait analisa topik atau trend penting yang dimunculkan pada dashboard web

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl 'http://localhost:3030/download/dashboard-usage?startDate=2020-04-19&endDate=2020-05-18' \
  -H 'Connection: keep-alive' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36' \
  -H 'Origin: http://localhost:4200' \
  -H 'Sec-Fetch-Site: same-site' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H 'Referer: http://localhost:4200/user/report-dashboard' \
  -H 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7' \
  --compressed
```
</details>

<details>

<summary>Response</summary>

Response dari request ini adalah unduhan berupa file xlsx dengan format sebagai berikut 

Sheet Dashboard Endpoint:

| Manage Endpoint | Active Endpoint |   |	
|-----------------|-----------------|---|
| 259| 0 |
|    |	 |	
| Detail Endpoint |		
| Name | Duration Minute | Active |
| VC LC PALEMBANG | 0 | No |

Sheet Dashboard Meeting:

| Minute Meeting | Count Meeting |   |   |   |   |   |   |   |
|----------------|---------------|---|---|---|---|---|---|---|
| 0.00 | 0 |							
|      |					
| Detail Meeting |								
| No | Date | Connected Time | Disconnected Time | Duration Minute | Caller | Receiver | Caller Device SN | Receiver Device SN |

Sheet top 10 Endpoint Count:

| No | Device Name | Location | Meeting Count |
|----|-------------|----------|---------------|

Sheet top 10 Endpoint Duration:

| No | Device Name | Location | Meeting Minute |
|----|-------------|----------|---------------|

Sheet top 5 Disconnect Reason

| No | Description | Count | 
|----|-------------|----------|

</details>

> /download/schedule-meeting

Digunakan untuk menguduh file report terkait jadwal meeting pada range date tertentu

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl 'http://localhost:3030/download/schedule-meeting?startDate=1587229200&endDate=1589821199' \
  -H 'Connection: keep-alive' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36' \
  -H 'Origin: http://localhost:4200' \
  -H 'Sec-Fetch-Site: same-site' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H 'Referer: http://localhost:4200/user/schedule-meeting' \
  -H 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7' \
  --compressed
```
</details>

<details>
<summary>Response</summary>

Response dari request ini adalah unduhan berupa file xlsx dengan format sebagai berikut 

Sheet Top 10 TMSXE:

| No | User | Duration | Occurences |
|----|------|----------|------------|

Sheet Top 10 TMS-SMARTSCHEDULER:

| No | User | Duration | Occurences |
|----|------|----------|------------|

Sheet Trend Schedule Meeting:

| Trend | TMS-SMARTSCHEDULER User | TMS-SMARTSCHEDULER Duration Hour | TMS-SMARTSCHEDULER Occurences | TMSXE User | TMSXE Duration Hour | TMSXE Occurences |
|-------|-------------------------|----------------------------------|-------------------------------|------------|---------------------|------------------|

</details>

> /download/teams-usage

Digunakan untuk mengunduh file report analisa penggunaan service teams baik itu service call, team chat, private chat maupun meeting.

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl 'http://localhost:3030/download/teams-usage?startDate=2020-05-18&endDate=undefined' \
  -H 'Connection: keep-alive' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36' \
  -H 'Origin: http://localhost:4200' \
  -H 'Sec-Fetch-Site: same-site' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H 'Referer: http://localhost:4200/user/report-teams' \
  -H 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7' \
  --compressed
```
</details>

<details>
<summary>Response</summary>

Response dari request ini adalah unduhan berupa file xlsx dengan format sebagai berikut 

Sheet Trend Teams Activity 30D Period:

| Last Updated | 2020-03-06 |		|   |   |
|--------------|------------|-------|---|---|
|Report Date | Team Chat | Private Chat | Call | Meeting |

Sheet Teams Device Distribution:

| Report Refresh Date | Web | Android Phone | iOS | Mac | Windows |
|---------------------|-----|---------------|-----|-----|---------|

Sheet Top 10 Teams Active User:

| No | User | Meeting Count|
|----|------|--------------|
| 1 |
| 2 |
| 3 |
| ...10 |
|   |		
| No | User | Call Count |
| 1 |
| 2 |
| 3 |
| ...10 |
|   |		
| No | User | Team Chat Count |
| 1 |
| 2 |
| 3 |
| ...10 |	
|   |	
| No | User | Private Chat Count |
| 1 |
| 2 |
| 3 |
| ...10 |

</details>

## Endpoint Capacity

### GET
> /endpoint-capacity

Menghasilkan nilai dari penggunaan endpoint-endpoint pada site tertentu dengan filter: site, dan range tanggal penggunaan

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| origDevicePoolName | string | kode pool atau site tertentu, jika tidak dicantumkan maka akan diambil semua sites | ya | "DP\_BDG" |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`  "result": {`|
|`    "DP_BDG": {`| site berdasarkan filter |
|`      "01-04-2020": {`| date berdasrkan filter |
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }        },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "cucmaxl": {        "name": "SEP881DFC610341",`|
|`        "description": "DX70 Bandung",`|
|`        "model": "Cisco DX70",`|
|`        "devicePoolName": "DP_BDG",`|
|`        "location": "Hub_None",`|
|`        "misconfigured": false,`|
|`        "bandwidth": "2048",`|
|`        "created": "2020-04-13T11:41:38.827Z",`|
|`        "updated": "2020-04-13T11:41:38.827Z",`|
|`        "deleted": null,`|
|`        "device": {          "id": "2",`|
|`          "model": "Cisco DX70",`|
|`          "minCapacity": 1,`|
|`          "maxCapacity": 1,`|
|`          "category": 0,`|
|`          "created": "2020-04-13T11:41:38.765Z",`|
|`          "updated": "2020-04-13T11:41:38.765Z",`|
|`          "deleted": null`|
|`        },`|
|`        "pool": {          "name": "DP_BDG",`|
|`          "description": "Bandung",`|
|`          "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`          "created": "2020-04-13T11:41:35.476Z",`|
|`          "updated": "2020-04-13T11:41:35.476Z",`|
|`          "deleted": null,`|
|`          "Area": {            "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "name": "Wilayah 1",`|
|`            "created": "2020-04-13T11:41:35.434Z",`|
|`            "updated": "2020-04-13T11:41:35.434Z"`|
|`          }        }`|
|`      },`|
|`      "lowUsage": {`| data yang masuk kategori penggunaan rendah |
|`          "data": [],`|
|`          "highest": {`|        
|`              "voice": 0,`|
|`              "video": 0,`|
|`              "presentation": 0,`|
|`              "totalUsage": 0`|
|`          }`|      
|`      },`|
|`      "normalUsage": {`|  data yang masuk kategori penggunaan normal |
|`        "data": [],`|
|`        "highest": {`|         
|`          "voice": 0,`|
|`          "video": 0,`|
|`          "presentation": 0,`|
|`          "totalUsage": 0`|
|`        }`|      
|`      },`|
|`      "highUsage": {`| data yang masuk kategori penggunaan tinggi | 
|`          "data": [`|
|`          {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          },`|
|`          {            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`            "usageStatus": "high"`|
|`          }        ],`|
|`        "highest": {`| penggunaan tertinggi pada tanggal tersebut |   
|`          "date": "01-04-2020",`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        }      },`|
|`      "totalEndpoints": 1,`|
|`      "totalNotEndpoints": 0,`|
|`      "highestPerDate": {`| penggunaan tertinggi di setiap tanggalnya |
|`          "01-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "02-04-2020": {          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "03-04-2020": {`|          
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "06-04-2020": {`|  
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "07-04-2020": {`|      
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        },`|
|`        "08-04-2020": {`|
|`          "voice": 310.2,`|
|`          "video": 107.89999999999999,`|
|`          "presentation": 220.60000000000005,`|
|`          "totalUsage": 638.7,`|
|`          "readableName": "DX70 Bandung",`|
|`          "callingPartyNumber": "1127",`|
|`          "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f",`|
|`          "usageStatus": "high"`|
|`        }`|      
|`      },`|
|`      "highestUsage": {`|
|`        "date": "01-04-2020",`|
|`        "voice": 310.2,`|
|`        "video": 107.89999999999999,`|
|`        "presentation": 220.60000000000005,`|
|`        "totalUsage": 638.7,`|
|`        "readableName": "DX70 Bandung",`|
|`        "callingPartyNumber": "1127",`|
|`        "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`      },`|
|`      "02-04-2020": {`|    
|`      "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|        
|`          },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "03-04-2020": {`|     
|`      "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|        
|`      },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "06-04-2020": {`|        
|`          "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }`|        
|`      },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "07-04-2020": {        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`          }        },`|
|`        "totalEndpoint": 1`|
|`      },`|
|`      "08-04-2020": {`|        
|`        "rooms": {`|
|`          "SEP881DFC610341": {`|
|`            "voice": 310.2,`|
|`            "video": 107.89999999999999,`|
|`            "presentation": 220.60000000000005,`|
|`            "totalUsage": 638.7,`|
|`            "readableName": "DX70 Bandung",`|
|`            "callingPartyNumber": "1127",`|
|`            "id": "42957939-6e7f-4f52-8956-cd0ad8985b3f"`|
|`              }`|        
|`          },`|
|`        "totalEndpoint": 1`|
|`          }`|    
|`      }`|
|`  },`|
|`  "weekdays": 6`| jumlah hari kerja dalam seminggu (sabtu minggu tidak dihitung) |
|`}`|

</details>

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/endpoint-capacity?origDevicePoolName=DP_BDG&startDate=2020-04-01&endDate=2020-04-08&paginate=false' \
--header 'Connection: keep-alive' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Origin: http://localhost:4200' \
--header 'Sec-Fetch-Dest: empty' \
--header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36' \
--header 'Sec-Fetch-Site: same-site' \
--header 'Sec-Fetch-Mode: cors' \
--header 'Referer: http://localhost:4200/endpoint-capacity' \
--header 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7' \
--header 'If-None-Match: W/"16b0-GhL5UznkJifNeelpKDcWQlhjTo0"'
```

</details>

## Endpoint Effectivity

### GET
> /endpoint-effectivity

Menghasilkan nilai people count tertinggi dari penggunaan endpoint pada range tanggal tertentu

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| endpoint | string | kode device atau endpoint | tidak | "SEP64F69D66BBE0" |
| date | string | format "YYYY-MM-DD", untuk mencari penggunaan tertinggi selama 1 hari | ya | "2020-04-01" |
| week | string | format "YYYY-MM-DD", untuk mencari penggunaan tertinggi selama 1 minggu | ya | "2020-04-18" |

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`  "date": "2020-04-01",`| date berdasarkan filter |
|`  "endpoint": "SEP84B802CD9058",`| endpoint berdasarkan filter |
|`  "data": {`| data pengguna pada jam tertentu |
|`    "9": [ ...`| data pengguna pada jam 09 |
|`      1,`|
|`      4`|
|`    ],`|
|`    "10": [`|
|`      1,`|
|`      5`|
|`    ],`|
|`    "11": [`|
|`      4,`|
|`      0`|
|`    ],`|
|`    "13": [`|
|`      5,`|
|`      3`|
|`    ],`|
|`    "14": [`|
|`      1,`|
|`      5`|
|`    ],`|
|`    "15": [`|
|`      2,`|
|`      0`|
|`    ],`|
|`    "16": [`|
|`      2,`|
|`      3`|
|`    ]`|
|`  },`|
|`  "highest": {`| data pengguna tertinggi tiap jam nya |
|`    "9": 4,`|
|`    "10": 5,`|
|`    "11": 4,`|
|`    "13": 5,`|
|`    "14": 5,`|
|`    "15": 2,`|
|`    "16": 3`|
|`  },`|
|`  "cucmaxl": {`| ditail cucm |
|`    "name": "SEP84B802CD9058",`|
|`    "description": "SX80 8451 KP 1",`|
|`    "model": "Cisco TelePresence SX80",`|
|`    "devicePoolName": "DP_JAKARTA01",`|
|`    "location": "LOC_JAKARTA01",`|
|`    "misconfigured": false,`|
|`    "bandwidth": "2048",`|
|`    "created": "2020-04-13T11:41:38.827Z",`|
|`    "updated": "2020-04-13T11:41:38.827Z",`|
|`    "deleted": null,`|
|`    "pool": {`|
|`      "name": "DP_JAKARTA01",`|
|`      "description": "Kantor Pusat 1",`|
|`      "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`      "created": "2020-04-13T11:41:35.476Z",`|
|`      "updated": "2020-04-13T11:41:35.476Z",`|
|`      "deleted": null`|
|`    }`|
|`  },`|
|`  "device": {`| ditail device
|`    "id": "45",`|
|`    "model": "Cisco TelePresence SX80",`|
|`    "minCapacity": 10,`|
|`    "maxCapacity": 20,`|
|`    "category": 1,`|
|`    "created": "2020-04-13T11:41:38.765Z",`|
|`    "updated": "2020-04-13T11:41:38.765Z",`|
|`    "deleted": null`|
|`  }`|
|`}`|

</details>

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/endpoint-effectivity?date=2020-04-01&endpoint=SEP84B802CD9058' \
--header 'Connection: keep-alive' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Origin: http://localhost:4200' \
--header 'Sec-Fetch-Dest: empty' \
--header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36' \
--header 'Sec-Fetch-Site: same-site' \
--header 'Sec-Fetch-Mode: cors' \
--header 'Referer: http://localhost:4200/endpoint-effectivity' \
--header 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7'
```
</details>

## Fetch

### PUT
> /fetch/cdr

Digunakan untuk memicu job [fetchCdr](/documentation/index.md#fetchcdr). dengan mengirimkan array dari pkid yang terbaru. Sebuah record jobs baru akan disimpan ke dalam tabel 'jobs'.

Body

```
{
	"pkid": [
		"622521c3-ae68-4fc7-8114-ef415aeffad6",
		"ec36b387-8ac3-42fd-b389-d3fa5e07932b",
		"7320ede0-e40e-4587-b1c7-b119c8941203"
	]
}
```

Curl

```
curl --location --request PUT 'http://localhost:3030/fetch/cdr' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
	"pkid": [
		"622521c3-ae68-4fc7-8114-ef415aeffad6",
		"ec36b387-8ac3-42fd-b389-d3fa5e07932b",
		"7320ede0-e40e-4587-b1c7-b119c8941203"
	]
}'
```

Response

```
{
    "message": "Your request has been received. The operation log will be stored at file: fetchCdr_2020-04-24_14-43-21.log"
}
```

Record Tabel Jobs

contoh record jobs yang dihasilkan di tabel 'jobs'

```
[
  {
    "id": "1",
    "task": "fetch-cdr",
    "parameter": {
      "startTime": "2020-04-24T07:43:21.519Z",
      "pkid": [
        "622521c3-ae68-4fc7-8114-ef415aeffad6",
        "ec36b387-8ac3-42fd-b389-d3fa5e07932b",
        "7320ede0-e40e-4587-b1c7-b119c8941203"
      ]
    },
    "created": "2020-04-24T07:43:21.534Z",
    "updated": "2020-04-24T07:43:21.534Z",
    "deleted": null
  }
]
```


### PUT
> /fetch/cmr


Digunakan untuk memicu job [fetchCmr](/documentation/index.md#fetchcmr). dengan mengirimkan array dari pkid yang terbaru. Sebuah record jobs baru akan disimpan ke dalam tabel 'jobs'.

Body

```
{
"pkid": [
"622521c3-ae68-4fc7-8114-ef415aeffad6",
"ec36b387-8ac3-42fd-b389-d3fa5e07932b",
"7320ede0-e40e-4587-b1c7-b119c8941203"
]
}
```

Curl

```
curl --location --request PUT 'http://localhost:3030/fetch/cmr' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
"pkid": [
"622521c3-ae68-4fc7-8114-ef415aeffad6",
"ec36b387-8ac3-42fd-b389-d3fa5e07932b",
"7320ede0-e40e-4587-b1c7-b119c8941203"
]
}'
```

Response

```
{
    "message": "Your request has been received. The operation log will be stored at file: fetchCmr_2020-04-24_15-26-03.log"
}
```

Record Tabel Jobs

contoh record jobs yang dihasilkan di tabel 'jobs'

```
[
  {
  "id": "2",
  "task": "fetch-cmr",
  "parameter": {
    "startTime": "2020-04-24T08:26:03.523Z",
    "pkid": [
      "622521c3-ae68-4fc7-8114-ef415aeffad6",
      "ec36b387-8ac3-42fd-b389-d3fa5e07932b",
      "7320ede0-e40e-4587-b1c7-b119c8941203"
    ]
  },
  "created": "2020-04-24T08:26:03.533Z",
  "updated": "2020-04-24T08:26:03.533Z",
  "deleted": null
  }
]
```

## License Capacity

### GET
> /license-capacity

Menghasilkan nilai penggunaan license pada range tanggal tertentu

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | anggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |
<br>

<details>
<summary>Response</summary>

```
{"existingLimit":5,"data":{}}
```
</details>


<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/license-capacity?startDate=2020-04-01&endDate=2020-04-08&paginate=false' \
--header 'Connection: keep-alive' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Origin: http://localhost:4200' \
--header 'Sec-Fetch-Dest: empty' \
--header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36' \
--header 'Sec-Fetch-Site: same-site' \
--header 'Sec-Fetch-Mode: cors' \
--header 'Referer: http://localhost:4200/license-capacity' \
--header 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7'
```
</details>

## Pools

Pools atau bisa juga disebut site merupakan titik dimana enpoint-enpoint dari satu wilayah didistribusikan

### GET
> /pools

Menghasilkan list semua data pools yang tersedia.

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| $limit | int | limit data yang diambil | ya | 10 |
| $skip | int | lewati data yang tidak diambil untuk pagination | tidak | 10 |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/pools'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`[ ...`| data dalam bentuk array |
|`{`| ditail pool |
|`        "name": "DP_BDG",`| nama (code) pool |
|`        "description": "Bandung",`|
|`        "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`| id area |
|`        "created": "2020-04-13T11:41:35.476Z",`| datetime data dibuat |
|`        "updated": "2020-04-13T11:41:35.476Z",`| datetime data terakhir diupdate |
|`        "deleted": null,`| dattime data dihapus |
|`        "cucmaxls": [ ...`| data cucmaxls yang berelasi dengan pool ini |
|`            {`| ditail cucmaxls |
|`                "name": "SEP881DFC610341",`|
|`                "description": "DX70 Bandung",`|
|`                "model": "Cisco DX70",`|
|`                "devicePoolName": "DP_BDG",`|
|`                "location": "Hub_None",`|
|`                "misconfigured": false,`|
|`                "bandwidth": "2048",`|
|`                "created": "2020-04-13T11:41:38.827Z",`|
|`                "updated": "2020-04-13T11:41:38.827Z",`|
|`                "deleted": null,`|
|`                "device": {`|
|`                    "id": "2",`|
|`                    "model": "Cisco DX70",`|
|`                    "minCapacity": 1,`|
|`                    "maxCapacity": 1,`|
|`                    "category": 0,`|
|`                    "created": "2020-04-13T11:41:38.765Z",`|
|`                    "updated": "2020-04-13T11:41:38.765Z",`|
|`                    "deleted": null`|
|`                },`|
|`                "pool": {`|
|`                    "name": "DP_BDG",`|
|`                    "description": "Bandung",`|
|`                    "area": "eafbd335-1275-4ac4-a92a-11c2f38b7`|e69",
|`                    "created": "2020-04-13T11:41:35.476Z",`|
|`                    "updated": "2020-04-13T11:41:35.476Z",`|
|`                    "deleted": null`|
|`                }`|
|`            }`|
|`        ],`|
|`        "Area": {`| data area yang berelasi dengan pool ini |
|`            "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "name": "Wilayah 1a",`|
|`            "created": "2020-04-13T11:41:35.434Z",`|
|`            "updated": "2020-04-27T03:32:57.741Z"`|
|`        }`|
|`    }`|
|`]`|

</details>

>

### PUT
> pools/:ID_POOL

Digunakan untuk mengubah semua data terkait pool yang dimaksud menggunakan ID pool nya

<details>
<summary>Body</summary>

```
{
	"name": "DP_JAKARTA01",
	"description": "Kantor Pusat 1"
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PUT 'http://localhost:3030/pools/DP_JAKARTA01' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
	"name": "DP_JAKARTA01",
	"description": "Kantor Pusat 1"
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "DP_JAKARTA01",
    "description": "Kantor Pusat 1",
    "area": null,
    "created": "2020-04-13T11:41:35.476Z",
    "updated": "2020-05-11T00:38:43.012Z",
    "deleted": null
}
```
</details>

### PATCH
> pools/:ID_POOL

Digunakan untuk mengubah sebagian data terkait pool yang dimaksud menggunakan ID pool nya

<details>
<summary>Body</summary>

```
{
	"name": "DP_JAKARTA01",
	"description": "Kantor Pusat 1"
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/pools/DP_JAKARTA01' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
	"name": "DP_JAKARTA01",
	"description": "Kantor Pusat 1"
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "DP_JAKARTA01",
    "description": "Kantor Pusat 1",
    "area": null,
    "created": "2020-04-13T11:41:35.476Z",
    "updated": "2020-05-11T00:38:43.012Z",
    "deleted": null
}
```
</details>

> pools

Digunakan untuk mengubah secara massive pool yang dimaksud menggunakan array pada body requestnya

<details>
<summary>Body</summary>

```
[
	{
		"name": "DP_JAKARTA01",
		"description": "Kantor Pusat 01"
	},
	{
		"name": "DP_JAKARTA02",
		"description": "Kantor Pusat 02"
	},
	{
		"name": "DP_JAKARTA03",
		"description": "Kantor Pusat 03"
	}
]
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/pools' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '[
	{
		"name": "DP_JAKARTA01",
		"description": "Kantor Pusat 01"
	},
	{
		"name": "DP_JAKARTA02",
		"description": "Kantor Pusat 02"
	},
	{
		"name": "DP_JAKARTA03",
		"description": "Kantor Pusat 03"
	}
]'
```
</details>

<details>
<summary>Response</summary>

```
[
    {
        "name": "DP_JAKARTA01",
        "description": "Kantor Pusat 01",
        "area": null,
        "created": "2020-04-13T11:41:35.476Z",
        "updated": "2020-05-11T00:46:10.449Z",
        "deleted": null
    },
    {
        "name": "DP_JAKARTA02",
        "description": "Kantor Pusat 02",
        "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",
        "created": "2020-04-13T11:41:35.476Z",
        "updated": "2020-05-11T00:46:10.451Z",
        "deleted": null
    },
    {
        "name": "DP_JAKARTA03",
        "description": "Kantor Pusat 03",
        "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",
        "created": "2020-04-13T11:41:35.476Z",
        "updated": "2020-05-11T00:46:10.459Z",
        "deleted": null
    }
]
```
</details>

## Return of Investment (ROI)

### GET
> /return-of-investment

menghasilkan nilai dari analisa efisiensi biaya meeting dengan menggunakan cisco teleconverence dibandingkan dengan biaya bertemu secara langsung

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| origDevicePoolName | string | kode sites untuk mengambil analisa berdasarkan site tertentu. Jika tidak dicantumkan maka akan diambil dari semua sites | ya | "DP\_BDG" |
| startDate | string | format "YYYY-MM-DD", filter batas awal tanggal | tidak | "2020-04-01" |
| endDate | string | format "YYYY-MM-DD", filter batas akhir tanggal | tidak | "2020-04-18" |

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`  "accomodations": [ ...`| biaya akomodasi untuk range tanggal filter |
|`    {`|
|`      "cost": "872038.00",`|
|`      "created": "2020-04-13T11:41:35.454Z",`|
|`      "updated": "2020-04-13T11:41:35.454Z",`|
|`      "from": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`| dari site |
|`      "to": "eafbd335-1275-4ac4-a92a-11c2f38b7e69"`| ke site yang lain |
|`    }`|
|`  ],`|
|`  "usages": [ ...`| penggunaan telekonferensi untuk range tanggal filter dalam bentuk array |
|`    {`| ditail penggunaan |
|`      "id": "94901878-5d6e-4ad5-a557-be7b2b71e478",`|
|`      "cucmaxlOrig": {`|
|`        "name": "SEPE4C7226502A6",`|
|`        "description": "EX90 Vicon Jakarta 1",`|
|`        "model": "Cisco TelePresence EX90",`|
|`        "devicePoolName": "DP_VICON_JAKARTA01",`|
|`        "location": "LOC_JAKARTA01",`|
|`        "misconfigured": false,`|
|`        "bandwidth": "2048",`|
|`        "created": "2020-04-13T11:41:38.827Z",`|
|`        "updated": "2020-04-13T11:41:38.827Z",`|
|`        "deleted": null,`|
|`        "device": {`|
|`          "id": "25",`|
|`          "model": "Cisco TelePresence EX90",`|
|`          "minCapacity": 0,`|
|`          "maxCapacity": 0,`|
|`          "category": 0,`|
|`          "created": "2020-04-13T11:41:38.765Z",`|
|`          "updated": "2020-04-13T11:41:38.765Z",`|
|`          "deleted": null`|
|`        },`|
|`        "pool": {`|
|`          "name": "DP_VICON_JAKARTA01",`|
|`          "description": "Vicon Jakarta 1",`|
|`          "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`          "created": "2020-04-13T11:41:35.476Z",`|
|`          "updated": "2020-04-13T11:41:35.476Z",`|
|`          "deleted": null,`|
|`          "Area": {`|
|`            "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "name": "Wilayah 1",`|
|`            "created": "2020-04-13T11:41:35.434Z",`|
|`            "updated": "2020-04-13T11:41:35.434Z"`|
|`          }`|
|`        }`|
|`      },`|
|`      "cucmaxlDest": {`|
|`        "name": "SEP00B0E15F3783",`|
|`        "description": "DX650 8212 KP 2",`|
|`        "model": "Cisco DX650",`|
|`        "devicePoolName": "DP_JAKARTA02",`|
|`        "location": "Hub_None",`|
|`        "misconfigured": false,`|
|`        "bandwidth": "2048",`|
|`        "created": "2020-04-13T11:41:38.827Z",`|
|`        "updated": "2020-04-13T11:41:38.827Z",`|
|`        "deleted": null,`|
|`        "device": {`|
|`          "id": "1",`|
|`          "model": "Cisco DX650",`|
|`          "minCapacity": 1,`|
|`          "maxCapacity": 1,`|
|`          "category": 0,`|
|`          "created": "2020-04-13T11:41:38.765Z",`|
|`          "updated": "2020-04-13T11:41:38.765Z",`|
|`          "deleted": null`|
|`        },`|
|`        "pool": {`|
|`          "name": "DP_JAKARTA02",`|
|`          "description": "Kantor Pusat 2",`|
|`          "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`          "created": "2020-04-13T11:41:35.476Z",`|
|`          "updated": "2020-04-13T11:41:35.476Z",`|
|`          "deleted": null,`|
|`          "Area": {`|
|`            "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "name": "Wilayah 1",`|
|`            "created": "2020-04-13T11:41:35.434Z",`|
|`            "updated": "2020-04-13T11:41:35.434Z"`|
|`          }`|
|`        }`|
|`      },`|
|`      "origArea": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`      "origAreaName": "Wilayah 1",`|
|`      "origPool": "Vicon Jakarta 1",`|
|`      "origDeviceName": "SEPE4C7226502A6",`|
|`      "origDevice": {`|
|`        "id": "25",`|
|`        "model": "Cisco TelePresence EX90",`|
|`        "minCapacity": 0,`|
|`        "maxCapacity": 0,`|
|`        "category": 0,`|
|`        "created": "2020-04-13T11:41:38.765Z",`|
|`        "updated": "2020-04-13T11:41:38.765Z",`|
|`        "deleted": null`|
|`      },`|
|`      "destArea": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`      "destAreaName": "Wilayah 1",`|
|`      "destPool": "Kantor Pusat 2",`|
|`      "destDeviceName": "SEP00B0E15F3783",`|
|`      "connectTime": "2020-04-01T09:00:07.000+07:00",`|
|`      "disconnectTime": "2020-04-01T11:25:58.000+07:00",`|
|`      "duration": "8751",`|
|`      "conventionalCost": {`|
|`        "transport": 0,`|
|`        "foodAndBeverage": 0,`|
|`        "residence": 0,`|
|`        "other": 0,`|
|`        "total": 1744076,`|
|`        "flightCost": 1744076`|
|`      },`|
|`      "teleconferenceCost": {`|
|`        "communicationLink": 0,`|
|`        "endpoint": 0,`|
|`        "telepresenceServer": 0,`|
|`        "total": 0`|
|`      },`|
|`      "passengers": 2,`|
|`      "readableDuration": "2h 25m 51s"`|
|`    }`|
|`  ],`|
|`  "total": {`| total biaya perbandingan |
|`    "conventionalCost": 8185820706,`| jika mengguakaan media konvensinal |
|`    "teleconferenceCost": 0`| jika menggunakan telekonferensi |
|`  }`|
|`}`|


</details>



<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/return-of-investment?origDevicePoolName=null&startDate=2020-04-01&endDate=2020-04-08&paginate=false' \
--header 'Connection: keep-alive' \
--header 'Accept: application/json, text/plain, */*' \
--header 'Origin: http://localhost:4200' \
--header 'Sec-Fetch-Dest: empty' \
--header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/80.0.3987.87 Chrome/80.0.3987.87 Safari/537.36' \
--header 'Sec-Fetch-Site: same-site' \
--header 'Sec-Fetch-Mode: cors' \
--header 'Referer: http://localhost:4200/roi' \
--header 'Accept-Language: id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7'
```
</details>

## Report Dashboard

### GET

> /report-dashboard

Digunakan untuk mengabil data terkait data-data yang akan ditampilkan ke dashboard user

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/report-dashboard'
```
</details>

<details>
<summary>Response</summary>

| Json | Deskripsi |
|------|-----------|
|`{`|
|`    "dashboardTrend": {`| data untuk dashboard trend |
|`        "manageEndpoint": 259,`| endpoint yang dikelola |
|`        "manageEndpointBefore": 259,`| jumlah endpoint sebelumnya |
|`        "activeEndpoint": 0,`| jumlah endpoint yan aktif |
|`        "activeEndpointBefore": 184,`| jumlah endpoint yang aktif sebelumnya |
|`        "durationMeeting": "0.00",`| durasi meeting |
|`        "durationMeetingBefore": "102310.90",`| durasi meeteng sebelumnya |
|`        "totalMeeting": 0,`| total meeting |
|`        "totalMeetingBefore": 1035,`| total meeting sebelumnya |
|`        "endpointDetail": [...`| ditail tiap endpoint, data dalam bentuk array | 
|`            {`|
|`                "name": "VC LC PALEMBANG",`| nama endpoint |
|`                "duration": 0,`| durasi penggunaan endpoint |
|`                "active": "No"`| status endpoint aktif atau tidak |
|`            }`|
|`        ],`|
|`        "meetingDetail": [...]`| ditail meeting dalam bentuk array |
|`    }`|
|`}`|
</details>

## Settings

### GET
> /settings/cucmaxl

Untuk mendapatkan value dari setting cucmaxl

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/cucmaxl'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`| ditail setting cucmaxl |
|`    "name": "cucmaxl",`| nama setting |
|`    "description": "Configuration for CUCMAXL",`| deskripsi setting |
|`    "configuration": {`|
|`        "uris": [`|
|`            "192.168.234.222:8443",`| url tempat pengambilan data |
|`            "192.168.234.223:8443"`| url tempat pengambilan data |
|`        ],`|
|`        "username": "axlapi",`| username untuk mengakses api |
|`        "password": "********"`| password untuk mengakses api |
|`    },`|
|`    "created": "2020-04-13T11:41:35.463Z",`| datetime setting ini dibuat |
|`    "updated": "2020-04-13T11:41:35.463Z"`| datetime setting ini diupdate |
|`}`|

</details>

> /settings/cmsapi-cospace

Untuk mendapatkan value dari setting cmsapi-cospace

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/cmsapi-cospace'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "name": "cmsapi-cospace",`| nama setting |
|`    "description": "Configuration for CMS API CoSpace",`| deskripsi setting |
|`    "configuration": {`|
|`        "uri": "192.168.234.231:445",`| url tempat pengambilan data |
|`        "username": "msxapi",`| username untuk mengakses api |
|`        "password": "********"`| password untuk mengakses api |
|`    },`|
|`    "created": "2020-04-13T11:41:35.463Z",`| datetime setting ini dibuat |
|`    "updated": "2020-04-13T11:41:35.463Z"`| datetime setting ini diupdate |
|`}`|

</details>

> /settings/cmsapi-license

Untuk mendapatkan value dari setting cmsapi-license

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/cmsapi-license'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "name": "cmsapi-license",`| nama setting |
|`    "description": "Configuration for CMS API License",`| deskripsi setting |
|`    "configuration": {`|
|`        "uri": "192.168.234.231:445",`| url tempat pengambilan data |
|`        "username": "msxapi",`| username untuk mengakses api |
|`        "password": "********",`| password untuk mengakses api |
|`        "limit": 5`| batas banyaknya data yang diambil |
|`    },`|
|`    "created": "2020-04-13T11:41:35.463Z",`| datetime setting ini dibuat |
|`    "updated": "2020-04-13T11:41:35.463Z"`| datetime setting ini diupdate |
|`}`|

</details>

> /settings/webex

Untuk mendapatkan value dari setting webex

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/webex'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "name": "webex",`| nama setting |
|`    "description": "Configuration for WebEx",`| deskripsi setting |
|`    "configuration": {`|
|`        "uri": "msiccmp.webex.com",`| url tempat pengambilan data |
|`        "site": "msiccmp",`|
|`        "webExId": "yasid.ramli@mastersystem.co.id",`| username untuk mengakses api |
|`        "password": "********"`| password untuk mengakses api |
|`    },`|
|`    "created": "2020-04-13T11:41:35.463Z",`| datetime setting ini dibuat |
|`    "updated": "2020-04-13T11:41:35.463Z"`| datetime setting ini diupdate |
|`}`|
</details>

> /settings/endpoint-cost

Untuk mendapatkan value dari setting endpoint-cost

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/endpoint-cost'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "endpoint-cost",
    "description": "endpoint-cost",
    "configuration": {
        "Cisco DX80": {
            "endpoint": "10000"
        },
        "Cisco DX70": {
            "endpoint": "200000"
        },
        "Cisco Spark Room Kit": {
            "endpoint": "3000"
        },
        "Cisco TelePresence DX70": {
            "communication": "4000000",
            "endpoint": "5000000",
            "telepresence": "0"
        },
        "Cisco TelePresence DX80": {
            "communication": "9000000",
            "endpoint": "1000000",
            "telepresence": "200000"
        },
        "Cisco TelePresence MX200 G2": {
            "communication": "1000000",
            "endpoint": "1000000",
            "telepresence": "1000000"
        },
        "Cisco TelePresence MX300 G2": {
            "communication": "2000000",
            "endpoint": "2000000",
            "telepresence": "2000000"
        },
        "Cisco TelePresence SX20": {
            "communication": "3000000",
            "endpoint": "3000000",
            "telepresence": "3000000"
        },
        "Cisco TelePresence SX80": {
            "communication": "4000000",
            "endpoint": "4000000",
            "telepresence": "4000000"
        },
        "Cisco Webex Room 55": {
            "communication": "5000000",
            "endpoint": "5000000",
            "telepresence": "5000000"
        }
    },
    "created": "2020-01-31T03:49:58.878Z",
    "updated": "2020-02-14T10:46:22.255Z"
}
```
</details>

> /settings/teams

Untuk mendapatkan value dari setting teams

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/teams'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "teams",
    "description": "Configuration for Microsoft Teams",
    "configuration": {
        "aadName": "",
        "clientId": "",
        "clientSecret": ""
    },
    "created": "2020-03-09T06:46:11.418Z",
    "updated": "2020-03-09T06:46:11.418Z"
}
```
</details>

> /settings/people-count

Untuk mendapatkan value dari setting people-count

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/people-count'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "people-count",
    "description": "Configuration for People Count device",
    "configuration": {
        "username": "",
        "password": ""
    },
    "created": "2020-02-21T03:22:10.568Z",
    "updated": "2020-02-21T03:22:10.568Z"
}
```
</details>

> /settings/accommodation-cost

Untuk mendapatkan value dari setting accommodation-cost

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/accommodation-cost'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "accommodation-cost",
    "description": "accomodation-cost",
    "configuration": {
        "residence": 400000,
        "foodAndBeverage": 400000,
        "transport": 400000,
        "other": 400000
    },
    "created": "2020-01-31T03:50:52.537Z",
    "updated": "2020-02-12T06:42:19.004Z"
}
```
</details>

> /settings/backend-mastersystem

Untuk mendapatkan value dari setting backend-mastersystem

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/backend-mastersystem'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "backend-mastersystem",
    "description": "backend mastersystem",
    "configuration": {
        "uri": "http://192.168.234.226"
    },
    "created": "2020-03-13T03:24:55.387Z",
    "updated": "2020-03-13T03:24:55.387Z"
}
```
</details>

> /settings/webex-token

Untuk mendapatkan value dari setting webex-token

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/webex-token'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "webex-token",
    "description": "webex-token config",
    "configuration": {
        "redirectUri": "https://herokuapp.com",
        "clientId": "5aac53ebca59ec175b78554d8745e81cbcbf3a241cfe",
        "clientSecret": "ed9aad8d8b053b781423f1a21d40c83d59b758e18d54eb8ab267a",
        "accessToken": "MjMxZDUwZTYtYTQ3Ny00MTk3LTg2NTEtYzQ0ZmQyMDg2ZWQxMTI4NzMyYmQtMjZh_PF84_18a7bab3-8072-ad6a-d2c44826aef8",
        "accessTokenExpDate": 1209599,
        "refreshToken": "NmQwNDA2MTItNDc4Yi00Yjg5LWFhNDgtNTVhNTA3Nzg1Y2RlY2M3NGI2YTYtNjQ0_PF84_18a7bab3-8072-414f-d2c44826aef8",
        "refreshTokenExpDate": 7775999
    },
    "created": "2020-02-04T04:22:34.537Z",
    "updated": "2020-02-04T04:22:34.537Z"
}
```
</details>

> /settings/telepresence-cost

Untuk mendapatkan value dari setting telepresence-cost

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/telepresence-cost'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "telepresence-cost",
    "description": "cost for telepresence server",
    "configuration": {
        "telepresenceServer": 5000000
    },
    "created": "2020-02-03T06:56:33.851Z",
    "updated": "2020-02-12T06:42:08.971Z"
}
```
</details>

> /settings/cms-multipartylicense

Untuk mendapatkan value dari setting cms-multipartylicense

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/settings/cms-multipartylicense'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "cms-multipartylicense",
    "description": "Configuration for CMS multi party license",
    "configuration": {
        "uris": [
            "192.168.192.48:445",
            "192.168.48.41:445",
            "192.168.40.48:445"
        ],
        "username": "",
        "password": "",
        "limit": 40
    },
    "created": "2020-03-19T10:12:26.636Z",
    "updated": "2020-03-19T10:12:26.636Z"
}
```
</details>

### PATCH
> /settings/cucmaxl

Untuk mengubah settings cucmaxl

<details>
<summary>Body</summary>

```
{
	"configuration": {
        "uris": [
            "192.168.234.222:8443",
            "192.168.234.223:8443"
        ],
        "username": "",
        "password": "",
        "uri": "https://192.168.234.222:8443"
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/cucmaxl' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
	"configuration": {
        "uris": [
            "192.168.234.222:8443",
            "192.168.234.223:8443"
        ],
        "username": "",
        "password": "",
        "uri": "https://192.168.234.222:8443"
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "cucmaxl",
    "description": "cucmaxl config",
    "configuration": {
        "uris": [
            "192.168.234.222:8443",
            "192.168.234.223:8443"
        ],
        "username": "",
        "password": "",
        "uri": "https://192.168.234.222:8443"
    },
    "created": "2020-02-04T04:22:58.420Z",
    "updated": "2020-05-26T04:55:56.332Z"
}
```
</details>

> /settings/cmsapi-cospace

Untuk mengubah settings cmsapi-cospace

<details>
<summary>Body</summary>

```
{
	"configuration": {
        "uri": "192.168.234.231:445",
        "username": "",
        "password": ""
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/cmsapi-cospace' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
	"configuration": {
        "uri": "192.168.234.231:445",
        "username": "",
        "password": ""
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "cmsapi-cospace",
    "description": "Configuration for CMS API CoSpace",
    "configuration": {
        "uri": "192.168.234.231:445",
        "username": "",
        "password": ""
    },
    "created": "2020-03-13T03:25:50.131Z",
    "updated": "2020-05-26T04:52:57.855Z"
}
```
</details>

> /settings/cmsapi-license

Untuk mengubah settings cmsapi-license

<details>
<summary>Body</summary>

```
{
"configuration": {
        "uri": "192.168.234.231:445",
        "username": "",
        "password": ""
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/cmsapi-license' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
"configuration": {
        "uri": "192.168.234.231:445",
        "username": "",
        "password": ""
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "cmsapi-license",
    "description": "cms api license",
    "configuration": {
        "uri": "192.168.234.231:445",
        "username": "",
        "password": ""
    },
    "created": "2020-02-10T22:01:37.547Z",
    "updated": "2020-05-26T04:58:12.490Z"
}
```
</details>

> /settings/webex

Untuk mengubah settings webex

<details>
<summary>Body</summary>

```
{
"configuration": {
        "uri": "webex.com",
        "site": "",
        "webExId": "",
        "password": ""
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/webex' \
--header 'Content-Type: application/json' \
--header 'Content-Type: text/plain' \
--data-raw '{
"configuration": {
        "uri": "webex.com",
        "site": "",
        "webExId": "",
        "password": ""
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "webex",
    "description": "webex config",
    "configuration": {
        "uri": "webex.com",
        "site": "com",
        "webExId": "",
        "password": ""
    },
    "created": "2020-02-04T04:22:15.021Z",
    "updated": "2020-05-26T04:59:52.929Z"
}
```
</details>

> /settings/endpoint-cost

Untuk mengubah settings endpoint-cost

<details>
<summary>Body</summary>

```
[
    {
        "model": "Cisco DX80",
        "cost": {
            "endpoint": "10000"
        }
    }
]
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/endpoint-cost' \
--header 'Content-Type: application/json' \
--data-raw '[
    {
        "model": "Cisco DX80",
        "cost": {
            "endpoint": "10000"
        }
    }
]'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "endpoint-cost",
    "description": "endpoint-cost",
    "configuration": {
        "Cisco DX80": {
            "endpoint": "10000"
        },
        "Cisco DX70": {
            "endpoint": "200000"
        },
        "Cisco Spark Room Kit": {
            "endpoint": "3000"
        },
        "Cisco TelePresence DX70": {
            "communication": "4000000",
            "endpoint": "5000000",
            "telepresence": "0"
        },
        "Cisco TelePresence DX80": {
            "communication": "9000000",
            "endpoint": "1000000",
            "telepresence": "200000"
        },
        "Cisco TelePresence MX200 G2": {
            "communication": "1000000",
            "endpoint": "1000000",
            "telepresence": "1000000"
        },
        "Cisco TelePresence MX300 G2": {
            "communication": "2000000",
            "endpoint": "2000000",
            "telepresence": "2000000"
        },
        "Cisco TelePresence SX20": {
            "communication": "3000000",
            "endpoint": "3000000",
            "telepresence": "3000000"
        },
        "Cisco TelePresence SX80": {
            "communication": "4000000",
            "endpoint": "4000000",
            "telepresence": "4000000"
        },
        "Cisco Webex Room 55": {
            "communication": "5000000",
            "endpoint": "5000000",
            "telepresence": "5000000"
        }
    },
    "created": "2020-01-31T03:49:58.878Z",
    "updated": "2020-05-26T05:10:22.839Z"
}
```
</details>

> /settings/people-count

Untuk mengubah settings people-count

<details>
<summary>Body</summary>

```
{
"configuration": {
        "username": "",
        "password": ""
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/people-count' \
--header 'Content-Type: application/json' \
--data-raw '{
"configuration": {
        "username": "",
        "password": ""
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "people-count",
    "description": "Configuration for People Count device",
    "configuration": {
        "username": "",
        "password": ""
    },
    "created": "2020-02-21T03:22:10.568Z",
    "updated": "2020-05-26T05:31:12.625Z"
}
```
</details>

> /settings/accommodation-cost

Untuk mengubah settings accommodation-cost

<details>
<summary>Body</summary>

```
{
"configuration": {
        "residence": 400000,
        "foodAndBeverage": 400000,
        "transport": 400000,
        "other": 400000
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/accommodation-cost' \
--header 'Content-Type: application/json' \
--data-raw '{
"configuration": {
        "residence": 400000,
        "foodAndBeverage": 400000,
        "transport": 400000,
        "other": 400000
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "accommodation-cost",
    "description": "accomodation-cost",
    "configuration": {
        "residence": 400000,
        "foodAndBeverage": 400000,
        "transport": 400000,
        "other": 400000,
        "configuration": {
            "residence": 400000,
            "foodAndBeverage": 400000,
            "transport": 400000,
            "other": 400000
        }
    },
    "created": "2020-01-31T03:50:52.537Z",
    "updated": "2020-05-26T05:36:19.695Z"
}
```
</details>

> /settings/backend-mastersystem

Untuk mengubah settings backend-mastersystem

<details>
<summary>Body</summary>

```
{
"configuration": {
        "uri": "http://192.168.234.226"
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/backend-mastersystem' \
--header 'Content-Type: application/json' \
--data-raw '{
"configuration": {
        "uri": "http://192.168.234.226"
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "backend-mastersystem",
    "description": "backend mastersystem",
    "configuration": {
        "uri": "http://192.168.234.226"
    },
    "created": "2020-03-13T03:24:55.387Z",
    "updated": "2020-05-26T07:13:41.046Z"
}
```
</details>

> /settings/webex-token

Untuk mengubah settings webex-token

<details>
<summary>Body</summary>

```
{
"configuration": {
        "redirectUri": "https://herokuapp.com",
        "clientId": "5aac53ebca59ec175b78554d8745e81cbcbf3a241cfe",
        "clientSecret": "ed9aad8d8b053b781423f1a21d40c83d59b758e18d54eb8ab267a",
        "accessToken": "MjMxZDUwZTYtYTQ3Ny00MTk3LTg2NTEtYzQ0ZmQyMDg2ZWQxMTI4NzMyYmQtMjZh_PF84_18a7bab3-8072-ad6a-d2c44826aef8",
        "accessTokenExpDate": 1209599,
        "refreshToken": "NmQwNDA2MTItNDc4Yi00Yjg5LWFhNDgtNTVhNTA3Nzg1Y2RlY2M3NGI2YTYtNjQ0_PF84_18a7bab3-8072-414f-d2c44826aef8",
        "refreshTokenExpDate": 7775999
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/webex-token' \
--header 'Content-Type: application/json' \
--data-raw '{
"configuration": {
        "redirectUri": "https://herokuapp.com",
        "clientId": "5aac53ebca59ec175b78554d8745e81cbcbf3a241cfe",
        "clientSecret": "ed9aad8d8b053b781423f1a21d40c83d59b758e18d54eb8ab267a",
        "accessToken": "MjMxZDUwZTYtYTQ3Ny00MTk3LTg2NTEtYzQ0ZmQyMDg2ZWQxMTI4NzMyYmQtMjZh_PF84_18a7bab3-8072-ad6a-d2c44826aef8",
        "accessTokenExpDate": 1209599,
        "refreshToken": "NmQwNDA2MTItNDc4Yi00Yjg5LWFhNDgtNTVhNTA3Nzg1Y2RlY2M3NGI2YTYtNjQ0_PF84_18a7bab3-8072-414f-d2c44826aef8",
        "refreshTokenExpDate": 7775999
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "webex-token",
    "description": "webex-token config",
    "configuration": {
        "redirectUri": "https://herokuapp.com",
        "clientId": "5aac53ebca59ec175b78554d8745e81cbcbf3a241cfe",
        "clientSecret": "ed9aad8d8b053b781423f1a21d40c83d59b758e18d54eb8ab267a",
        "accessToken": "MjMxZDUwZTYtYTQ3Ny00MTk3LTg2NTEtYzQ0ZmQyMDg2ZWQxMTI4NzMyYmQtMjZh_PF84_18a7bab3-8072-ad6a-d2c44826aef8",
        "accessTokenExpDate": 1209599,
        "refreshToken": "NmQwNDA2MTItNDc4Yi00Yjg5LWFhNDgtNTVhNTA3Nzg1Y2RlY2M3NGI2YTYtNjQ0_PF84_18a7bab3-8072-414f-d2c44826aef8",
        "refreshTokenExpDate": 7775999
    },
    "created": "2020-02-04T04:22:34.537Z",
    "updated": "2020-05-26T08:04:36.031Z"
}
```
</details>

> /settings/telepresence-cost

Untuk mengubah settings telepresence-cost

<details>
<summary>Body</summary>

```
{
	"configuration": {
        "telepresenceServer": 5000000
    }
}
```
</details>

<details>
<summary>Curl</summary>

```
curl --location --request PATCH 'http://localhost:3030/settings/telepresence-cost' \
--header 'Content-Type: application/json' \
--data-raw '{
	"configuration": {
        "telepresenceServer": 5000000
    }
}'
```
</details>

<details>
<summary>Response</summary>

```
{
    "name": "telepresence-cost",
    "description": "cost for telepresence server",
    "configuration": {
        "telepresenceServer": 5000000,
        "configuration": {
            "telepresenceServer": 5000000
        }
    },
    "created": "2020-02-03T06:56:33.851Z",
    "updated": "2020-05-26T08:09:03.432Z"
}
```
</details>

## Sites

### GET
> /sites
Untuk mendapatkan list data sites

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| boardOnly | boolean | untuk mengambil sites dengan board only | ya | true |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | ya | "2020-04-18" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | ya | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/sites?boardOnly=true&startDate=2019-05-01&endDate=2019-05-10'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`[ ...`| data sites dalam bentuk array |
|`    {`| ditail site |
|`        "devicePoolName": "DP_JAKARTA01",`|
|`        "name": "SEP84B802CD9058",`|
|`        "description": "SX80 8451 KP 1",`|
|`        "location": "LOC_JAKARTA01",`|
|`        "bandwidth": "2048",`|
|`        "device": {`|
|`            "id": "45",`|
|`            "model": "Cisco TelePresence SX80",`|
|`            "minCapacity": 10,`|
|`            "maxCapacity": 20,`|
|`            "category": 1,`|
|`            "created": "2020-04-13T11:41:38.765Z",`|
|`            "updated": "2020-04-13T11:41:38.765Z",`|
|`            "deleted": null`|
|`        },`|
|`        "pool": {`|
|`            "name": "DP_JAKARTA01",`|
|`            "description": "Kantor Pusat 1",`|
|`            "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "created": "2020-04-13T11:41:35.476Z",`|
|`            "updated": "2020-04-13T11:41:35.476Z",`|
|`            "deleted": null,`|
|`            "Area": {`|
|`                "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`                "name": "Wilayah 1a",`|
|`                "created": "2020-04-13T11:41:35.434Z",`|
|`                "updated": "2020-04-27T03:32:57.741Z"`|
|`            }`|
|`        }`|
|`    },`|
|`    {`|
|`        "devicePoolName": "DP_VICON_JAKARTA01",`|
|`        "name": "SEP64F69D66BBE0",`|
|`        "description": "SX80 Vicon Jakarta 1",`|
|`        "location": "LOC_JAKARTA01",`|
|`        "bandwidth": "2048",`|
|`        "device": {`|
|`            "id": "45",`|
|`            "model": "Cisco TelePresence SX80",`|
|`            "minCapacity": 10,`|
|`            "maxCapacity": 20,`|
|`            "category": 1,`|
|`            "created": "2020-04-13T11:41:38.765Z",`|
|`            "updated": "2020-04-13T11:41:38.765Z",`|
|`            "deleted": null`|
|`        },`|
|`        "pool": {`|
|`            "name": "DP_VICON_JAKARTA01",`|
|`            "description": "Vicon Jakarta 1",`|
|`            "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "created": "2020-04-13T11:41:35.476Z",`|
|`            "updated": "2020-04-13T11:41:35.476Z",`|
|`            "deleted": null,`|
|`            "Area": {`|
|`                "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`                "name": "Wilayah 1a",`|
|`                "created": "2020-04-13T11:41:35.434Z",`|
|`                "updated": "2020-04-27T03:32:57.741Z"`|
|`            }`|
|`        }`|
|`    }`|
|`]`|

</details>

> /sites/:sitesID

Untuk mendapatkan data endpoint pada sites tertentu berdasarkan code sites nya

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| boardOnly | boolean | untuk filter sites yang board only | ya | true |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/sites/DP_JAKARTA01?boardOnly=true'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`[ ...`| list data endpoint dalam bentuk array |
|`    {`|
|`        "name": "SEP84B802CD9058",`|
|`        "description": "SX80 8451 KP 1",`|
|`        "model": "Cisco TelePresence SX80",`|
|`        "devicePoolName": "DP_JAKARTA01",`|
|`        "location": "LOC_JAKARTA01",`|
|`        "misconfigured": false,`|
|`        "bandwidth": "2048",`|
|`        "created": "2020-04-13T11:41:38.827Z",`|
|`        "updated": "2020-04-13T11:41:38.827Z",`|
|`        "deleted": null,`|
|`        "device": {`|
|`            "id": "45",`|
|`            "model": "Cisco TelePresence SX80",`|
|`            "minCapacity": 10,`|
|`            "maxCapacity": 20,`|
|`            "category": 1,`|
|`            "created": "2020-04-13T11:41:38.765Z",`|
|`            "updated": "2020-04-13T11:41:38.765Z",`|
|`            "deleted": null`|
|`        },`|
|`        "pool": {`|
|`            "name": "DP_JAKARTA01",`|
|`            "description": "Kantor Pusat 1",`|
|`            "area": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`            "created": "2020-04-13T11:41:35.476Z",`|
|`            "updated": "2020-04-13T11:41:35.476Z",`|
|`            "deleted": null,`|
|`            "Area": {`|
|`                "id": "eafbd335-1275-4ac4-a92a-11c2f38b7e69",`|
|`                "name": "Wilayah 1a",`|
|`                "created": "2020-04-13T11:41:35.434Z",`|
|`                "updated": "2020-04-27T03:32:57.741Z"`|
|`            }`|
|`        }`|
|`    }`|
|`]`|

</details>

## Teams usage

### GET

> /teams-usage

Digunakan untuk mendapatkan data guna ditampilkan pada dasboard teams-usage

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-01" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | tidak | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/teams-usage'
```
</details>


<details>
<summary>Response</summary>

| Json | Deskripsi |
|------|-----------|
|`{`|
|`    "teamsDevices": [ ...`| laporan penggunaan sofware teams per tipe devicenya |
|`        {`|
|`            "id": "5",`|
|`            "reportRefreshDate": "2020-03-06T00:00:00.000Z",`|
|`            "web": "8",`|
|`            "windowsPhone": "0",`|
|`            "androidPhone": "38",`|
|`            "ios": "15",`|
|`            "mac": "4",`|
|`            "windows": "105",`|
|`            "reportPeriod": "30",`|
|`            "created": "2020-03-09T04:04:17.423Z",`|
|`            "updated": "2020-03-09T04:04:17.423Z",`|
|`            "deleted": null`|
|`        }`|
|`    ],`|
|`    "teamsUser": {`|
|`        "topUserMeeting": [ ...`| 10 top pengguna teams meeting |
|`            {`|
|`                "user": "samuel.simangunsong@mastersystem.co.id",`|
|`                "value": 9`|
|`            }`|
|`        ],`|
|`        "topUserCall": [ ...`| 10 top pengguna teams call |
|`            {`|
|`                "user": "tri.angga@mastersystem.co.id",`|
|`                "value": 8`|
|`            }`|
|`        ],`|
|`        "topUserTeamChat": [ ...`| 10 top user pengguna teams chat |
|`            {`|
|`                "user": "rio.kurnia@mastersystem.co.id",`|
|`                "value": 23`|
|`            }`|
|`        ],`|
|`        "topUserPrivateChat": [ ...`| 10 top user pengguna private chat |
|`            {`|
|`                "user": "abdul.aziz@mastersystem.co.id",`|
|`                "value": 35`|
|`            }`|
|`        ]`|
|`    },`|
|`    "teamsSum": [ ...`| total teams |
|`        {`|
|`            "totalUser": "1726"`|
|`        }`|
|`    ],`|
|`    "teamsActivity": [ ...`| aktifitas teams selama range date filter |
|`        {`|
|`            "reportRefreshDate": "2020-03-06T00:00:00.000Z",`|
|`            "reportDate": "2020-02-06T00:00:00.000Z",`|
|`            "teamChat": 2,`|
|`            "privateChat": 1,`|
|`            "call": 2,`|
|`            "meeting": 0`|
|`        }`|
|`    ],`|
|`    "teamsPassiveUser": [ ...`| user teams yang tidak ada aktifitas |
|`        {`|
|`            "user": "fadhillah.rasyadah@mastersystem.co.id",`|
|`            "lastActivity": "2019-11-20T00:00:00.000Z"`|
|`        }`|
|`    ]`|
|`}`|

</details>

## Webex Usage

### GET
> /webex-usage

| parameter | tipe | deskripsi | opsional | contoh |
| --------- | ---- | --------- | -------- | ------ |
| source | string |  | ya | true |
| startDate | string | tanggal awal filter pencarian dengan format "YYYY-MM-DD" | ya | "2020-04-18" |
| endDate | string | tanggal akhir filter pencarian dengan format "YYYY-MM-DD" | ya | "2020-04-18" |

<details>
<summary>Curl</summary>

```
curl --location --request GET 'http://localhost:3030/webex-usage?startDate=2019-06-01&endDate=2019-06-08&source=webex'
```
</details>

<details>
<summary>Response</summary>

|json|deskripsi|
|----|-----------|
|`{`|
|`    "trends": [ ...`| data trend pengguna webex dalam bentuk array | 
|`        {`|
|`            "trendTime": "2020-03",`|
|`            "hostUser": 13,`|
|`            "activeUser": null,`|
|`            "meetingMinutes": null,`|
|`            "hostingCount": null,`|
|`            "participantCount": null,`|
|`            "participantMinutes": null,`|
|`            "totalHostUser": null`|
|`        },`|
|`        {`|
|`            "trendTime": "2020-02",`|
|`            "hostUser": 8,`|
|`            "activeUser": null,`|
|`            "meetingMinutes": null,`|
|`            "hostingCount": null,`|
|`            "participantCount": null,`|
|`            "participantMinutes": null,`|
|`            "totalHostUser": null`|
|`        }`|
|`    ],`|
|`    "webexData": {`| data webex |
|`        "meetingDurations": {`| kesimpulan pengguna |
|`            "summary": {`|
|`                "<15": 0,`|
|`                "15-30": 0,`|
|`                "30-60": 0,`|
|`                "60-120": 0,`|
|`                ">=120": 0`|
|`            },`|
|`            "transactions": {`| data pengguna per tanggalnya |
|`                "2019-06-03": {`|
|`                    "<15": 0,`|
|`                    "15-30": 0,`|
|`                    "30-60": 0,`|
|`                    "60-120": 0,`|
|`                    ">=120": 0`|
|`                },`|
|`                "2019-06-04": {`|
|`                    "<15": 0,`|
|`                    "15-30": 0,`|
|`                    "30-60": 0,`|
|`                    "60-120": 0,`|
|`                    ">=120": 0`|
|`                },`|
|`                "2019-06-05": {`|
|`                    "<15": 0,`|
|`                    "15-30": 0,`|
|`                    "30-60": 0,`|
|`                    "60-120": 0,`|
|`                    ">=120": 0`|
|`                },`|
|`                "2019-06-06": {`|
|`                    "<15": 0,`|
|`                    "15-30": 0,`|
|`                    "30-60": 0,`|
|`                    "60-120": 0,`|
|`                    ">=120": 0`|
|`                },`|
|`                "2019-06-07": {`|
|`                    "<15": 0,`|
|`                    "15-30": 0,`|
|`                    "30-60": 0,`|
|`                    "60-120": 0,`|
|`                    ">=120": 0`|
|`                }`|
|`            }`|
|`        },`|
|`        "meetingParticipants": {`| data participant per tanggalnya |
|`            "2019-06-03": {`|
|`                "1-4": 0,`|
|`                "5-8": 0,`|
|`                "9-12": 0,`|
|`                ">12": 0`|
|`            },`|
|`            "2019-06-04": {`|
|`                "1-4": 0,`|
|`                "5-8": 0,`|
|`                "9-12": 0,`|
|`                ">12": 0`|
|`            },`|
|`            "2019-06-05": {`|
|`                "1-4": 0,`|
|`                "5-8": 0,`|
|`                "9-12": 0,`|
|`                ">12": 0`|
|`            },`|
|`            "2019-06-06": {`|
|`                "1-4": 0,`|
|`                "5-8": 0,`|
|`                "9-12": 0,`|
|`                ">12": 0`|
|`            },`|
|`            "2019-06-07": {`|
|`                "1-4": 0,`|
|`                "5-8": 0,`|
|`                "9-12": 0,`|
|`                ">12": 0`|
|`            }`|
|`        },`|
|`        "hostWebExIds": []`| id host webex |
|`    }`|
|`}`|

</details>



